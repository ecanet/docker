# evenfunction
## @edt ASIX-M05 Curs 2021-2022


### Exemple even function només per al localhost


Implementar un servei de xarxa que indica si un vaor rebut és parell o no.

S'implementa en Python un servidor:
```
from xmlrpc.server import SimpleXMLRPCServer

def is_even(n):
    return n % 2 == 0

server = SimpleXMLRPCServer(("localhost", 8000))
print("Listening on port 8000...")
server.register_function(is_even, "is_even")
server.serve_forever()
```


I s'implementa també un client per accedir a aquest servidor
```
import xmlrpc.client

with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
    print("3 is even: %s" % str(proxy.is_even(3)))
    print("100 is even: %s" % str(proxy.is_even(100)))
```

Execució:

```
# Server
$ docker run --rm -v $(pwd):/app  python:alpine python /app/pyserver.py
```

```
# Altra consola

$ docker ps
CONTAINER ID   IMAGE           COMMAND                  CREATED              STATUS              PORTS     NAMES
baea923e1fab   python:alpine   "python /app/pyserve…"   About a minute ago   Up About a minute             zealous_rhodes

$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-06-04 19:13 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.00014s latency).
All 1000 scanned ports on ldap.edt.org (172.17.0.2) are closed
```

Iniciar una sessió dins el container dle server i provar que funciona el client connectant localment
al localhost.
```
$ docker exec -it zealous_rhodes /bin/bash
OCI runtime exec failed: exec failed: container_linux.go:367: starting container process caused: exec: "/bin/bash": stat /bin/bash: no such file or directory: unknown
[ecanet@mylaptop evenfunction]$ docker exec -it zealous_rhodes /bin/sh
/ # cd /app/
/app # python pyclient.py
3 is even: False
100 is even: True
```


En canvi connectant remotament no funciona. Si s'engega un container client i intenta connectar
amb el servidor, indicant la seva adreça, no funciona perquè el servidor només s'ha 
engegat per la ip *"localhost"*.

Aquest exemple falla perquè el client remot no pot contactar amb el port 8000 del servidor de la
seva adreça IP 172.17.0.2 perquè el programa python servidor no fa el **'bind'** a totes les seves
adreçes IP sinó només al *localhost*.

```
$ docker run --rm -v $(pwd):/app python:alpine python /app/pyclient-remot.py
Traceback (most recent call last):
  File "/app/pyclient-remot.py", line 6, in <module>
    print("3 is even: %s" % str(proxy.is_even(3)))
  File "/usr/local/lib/python3.10/xmlrpc/client.py", line 1122, in __call__
    return self.__send(self.__name, args)
```

#### Exemple even function per a totes les adreçes IP del host servidor

Aquest exemple rectifica el codi del servidor per fer que el programa python i la seva
funció *even* escolti per totes les adreces IP del host, fa el **bind** a *'0.0.0.0'*.

```
# Servidor 
$ docker run --rm -v $(pwd):/app  python:alpine python /app/pyserver-allip.py
```

Des de qualsevol altre host el programa python client pot connectar al port 8000
del servidor per la seva adreça IP *'pública'* 172.17.0.2.

Ara qualsevol client que es desplegui i tingui connectivitat amb el servidor pot demanar
si un número és parell o no.
```
$ docker run --rm -v $(pwd):/app python:alpine python /app/pyclient-remot.py
3 is even: False
100 is even: True
```


#### Exemple usant una xarxa pròpia


```
# Server

$ docker run --rm -v $(pwd):/app --net mynet  --network-alias pyserver python:alpine python /app/pyserver-allip.py
```

```
# Client

$ docker run --rm -v $(pwd):/app --net mynet  python:alpine python /app/pyclient-net.py
3 is even: False
100 is even: True
```

