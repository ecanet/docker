# docker
## @edt ASIX-M05 Curs 2021-2022


### ENTRYPOINT

Si en la imatge s'ha definit un entrypoint amb *date* ara el 
container **sempre** executa l'ordre date, sempre. Si no es passa cap 
command al final de l'ordre *docker run* evidentment fa el date. 

Si es passen altres ordres com per exemple *cal* o */bin/bash* fallen,
perquè s'interpreten com a arguments que es passen a l'rodre date.

És a dir, quan hi ha un entrypoint l'ordre que s'indica al final del *docker run* no és una ordre sinó arguments a passar a l'entrypoint.

```
$ docker build -t testentry .

$ docker run --rm -it testentry 
Tue Jun 28 17:25:42 UTC 2022
 
$ docker run --rm -it testentry cal
date: invalid date 'cal'

$ docker run --rm -it testentry /bin/bash
date: invalid date '/bin/bash'
```

```
$ docker run --rm -it testentry +%F
2022-06-28

$ docker run --rm -it testentry +"Avui és: %Y-%m-%d %H:%M"
Avui és: 2022-06-28 17:27
```

**Modificar l'entrypoint**

Es pot 'saltar' l'entrypoint en la comanda *docker run* usant justament aquesta mateixa opció.

```
$ docker run --rm --entrypoint /bin/bash -it testentry 
root@0ebf1efb1938:/# 

$ docker run --rm --entrypoint cal -it testentry 
     June 2022        
Su Mo Tu We Th Fr Sa  
          1  2  3  4  
 5  6  7  8  9 10 11  
12 13 14 15 16 17 18  
19 20 21 22 23 24 25  
26 27 28 29 30        
                      
$ docker run --rm --entrypoint cal -it testentry  07 2022
     July 2022        
Su Mo Tu We Th Fr Sa  
                1  2  
 3  4  5  6  7  8  9  
10 11 12 13 14 15 16  
17 18 19 20 21 22 23  
24 25 26 27 28 29 30  
31                    
```



