# docker
## @edt ASIX-M05 Curs 2021-2022

# Comptador de visites

En aquest apartat s'implementa l'exemple de Docker Getting Started de Docker-Compose que és un comptador de visites d'un servei web, usant flask i redis.

Get Started with Docker Compose:
 [Docker documentation](]https://docs.docker.com/compose/gettingstarted/)

 1. Setup.
 2. Create a Dockerfile.
 3. Define services in a compose file.
 4. Build and run your app.
 5. Edit the compose file to add a bind mount.
 6. Rebuild and run the app with compose.
 7. Update the application.
 8. Experiment with some other commands.

Components de l’aplicació:
 * **app.py**: aplicació en python que utilitza flask i redis per mostrar una pàgina web amb un comptador de visites.
 * **requirements.txt**: descripció dels requeriments de python per a funcionar. Necessari en fer el pip install.
 * **Dockerfile**: per a la creació de la imatge de la app de python.
 * **docker-compose.yml**: per al desplegament de dos serveis, serveu web (la app) i el servei redis.

## Part 1-4: Setup - Create - Define - Build

Es desplegarà amb docker compose dos serveis: web i redis. El servei web és l’aplicació python que utilitzant flask mostra una pàgina web amb un comptador de visites. El fitxer Dockerfile és el que descriu com crear la imatge de l’aplicació (no té nom). Utilitza app.py i requirements.txt, pip i es basa en una imatge de Alpine Linux. L’altre servei es diu redis i es basa directament en l’imatge estàndard de Redis.

app.py
```
import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```

requirements.txt
```
flask
redis
```

Dockerfile
```
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["flask", "run"]
```

docker-compose.yml
```
version: '3'
services:
  web:
    build: .
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
```

Desplegament i execució
```
docker-compose up

<browser> http://localhost:5000
wget  http://localhost:5000 

docker image ls
docker ps
docker-compose ps
docker-compose down
docker-compose -d up
```

Es mostrarà la pàgina web amb el missatge “Hello World” i un comptador de visites que es va incrementant a cada nova visita o refresc de la pàgina.

![image1](images/image1.png)


## Part 5-6: Edit - Rebuild - Update

Aquesta part té per objectiu fer la aplicació ‘interactiva’.  De manera que es poden fer canvis ‘en calent’ a l’aplicació i aquests es veuen refectits immediatament. Per fer això es munta el directori de desenvolupament (de fora) al directori /code (de dins).

Es modifica el fitxer de docker-compose per fer un bind-mount de manera que es munta el directori actiu (el de desenvolupament de l’aplicació) al directori /code de dins de l’aplicació. També es defineix la variable de flask per indicar-li que ha d’actuar en mode desenvolupament (en aquest mode es recarrega cada vegada que es modifica la aplicació).

Es torna a fer el desplegament de l’aplicació amb “docker-compose up” i  observar que ara editant el fitxer app.py i per exemple canviant el missatge, aquest canvi es veu reflectit en la web.

docker-compose.yaml
```
version: '3'
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
```

rebuild
```
docker-compose up 
```

update
```
Edit app.py “Hello World from Docker”
<browser> refresh http://localhost:5000
```

```
docker-compose up -d
docker -compose ps

docker-compose run web env
docker-compose stop
docker -compose down --volumes
```


