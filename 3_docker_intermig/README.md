# docker
## @edt ASIX-M05 Curs 2021-2022

# Docker intermig

 * **compose** Generar desplegaments de varis containers de manera conjuntta formant una app. S'utilitza el format de fitxer YAML per descriure els serveis, volums i xarxes que n'han de formar part. Aprendrem a convertir les ordres docker run en fitxers de docker-compose.

 * **deploy** Per poder desplegar aplicacions amb múltiples rèpliques i a diversos nodes aprendrem a crear un *swarm* (encara que sigui d'un node) i desplegar-hi aplicacions. Ara les aplicacions les gestionem amb *docker stack* i els serveis que les componen amb *docker service*.

 * **swarm** Per poder desplegar aplicacions en un o més hots, anomenats *nodes* usarem *docker swarm* per gestionar aquests nodes. 


