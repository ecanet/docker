# docker
## @edt ASIX-M05 Curs 2021-2022

# Imatges personalitzades

Un dels objectius principals de treballar amb Docker és generar les nostres pròpies imatges personalitzades amb el sistema operatiu de base i els paquets i aplicacions que vulguem. 

És a dir, a part de poder usar multitud d’imatges ja existents proporcionades per tercers i disponibles al DockerHUb al Cloud, podem crear les nostres propies imatges i publicar-les també al Cloud (públicament o privadament).

Existeixen dos mecanismes per generar imatges:

  * **docker commit <container> <nom-nova-imatge>**
    
    Interactivament creant un container i moldejar-lo per tal de que contingui i faci allò que es vulgui. Un cop fet es genera una imatge a partir del container amb l’ordre docker commit.

  * **docker build -t nom-nova.imatge .**

    Aquest mecanisme utilitza un fitxer que descriu com una recepta (com un script) els passos necessaris per generar la imatge. Aquest fitxer té un llenguatge propi i s’anomena Dockerfile. Aquest és el mètode estàndard per generar imatges (el que cal usar). Nota: el punt al final és important!


## Crear un Fedora 32 personalitzat amb docker commit

El procediment que seguirem serà generar allò que volem que sigui la imatge partint d’una imatge prefabricada estàndard, instal·lar-hi els paquets que facin falta i un cop tenim que el container té tot aquell software que volem generar-ne una imatge personalitzada amb docker commit.

```
docker commit <ontainer> <name-new-image>
```

En aquest apartat farem la pràctica de generar una imatge de gNU/Linux basada en Fedora 32 personalitzada amb un conjunt de paquets que ens permetin fer tasques usuals d’examinar processos, ports, adreces IP, etc.

Hem vist en exemples anteriors que les imatges públiques de sistemes operatius GNU/Linux són molt pettites i per aconseguir-ho contenen pocs paquets. O dir d’una altra manera, a les imatges li falten paquets hi hi ha moltes ordres que no es poden realitzar.

Així per exemple hem vist que en un  Fedora:32 no podem fer ordres com per exemple:

  * **ps ax** per llistar els processos.
  * **nmap localhost** per observar els ports.
  * **ip a** per observar les interfícies i les adreeces de xarxa.
  * **ping** per poder verificar la connectivitat de xarxa.
  * **tree** per llistar directoris.
  * **vim** per editar fitxers.

```
$ docker run --rm --name myfedora -h myfedora  -it fedora:32 /bin/bash

[root@myfedora /]# ps ax
bash: ps: command not found

[root@myfedora /]# nmap localhost
bash: nmap: command not found

[root@myfedora /]# ip a
bash: ip: command not found

[root@myfedora /]# ping 1.1.1.1
bash: ping: command not found

[root@myfedora /]# tree /tmp
bash: tree: command not found

[root@myfedora /]# vim carta.txt
bash: vim: command not found
```

El nostre objectiu serà preparar interactivament un container amb tots aquells paquets que volem que tingui el producte final. Axí podem anar fent proves pel mètode de prova i error i anar verificant que acabem obtenint un sistema operatiu amb tot allò que ens fa falta.

En aquest exemple li afegirem al **Fedora:32** els paquets necessàris per poder fer totes les ordres anteriors:

  * **procps** per poder fer l’ordre ps ax
  * **nmap** per poder fer l’ordre nmap.
  * **iproute** per poder fer l’ordre ip a.
  * **iputils** per poder fer ping
  * **tree** per poder fer l’ordre tree.
  * **vim** per poder editar fitxers.

```
[root@myfedora /]# yum -y install procps nmap iproute iputils tree vim
Fedora 32 openh264 (From Cisco) - x86_64                                                                                      1.8 kB/s | 2.5 kB     00:01    
Fedora Modular 32 - x86_64                                                                                                    1.7 MB/s | 4.9 MB     00:02    
Fedora Modular 32 - x86_64 - Updates                                                                                          2.0 MB/s | 4.6 MB     00:02    
Fedora 32 - x86_64 - Updates                                                                                                  4.5 MB/s |  30 MB     00:06    
Fedora 32 - x86_64                                                                                                            7.3 MB/s |  70 MB     00:09    
Dependencies resolved.
==============================================================================================================================================================
 Package                                 Architecture                    Version                                       Repository                        Size
==============================================================================================================================================================
Installing:
 iproute                                 x86_64                          5.9.0-1.fc32                                  updates                          676 k
 iputils                                 x86_64                          20200821-1.fc32                               updates                          167 k
 nmap                                    x86_64                          2:7.80-4.fc32                                 updates                          5.4 M
 procps-ng                               x86_64                          3.3.16-2.fc32                                 updates                          336 k
 tree                                    x86_64                          1.8.0-4.fc32                                  fedora                            56 k
 vim-enhanced                            x86_64                          2:8.2.2787-1.fc32                             updates                          1.6 M
Installing dependencies:
 gpm-libs                                x86_64                          1.20.7-21.fc32                                fedora                            20 k
 libssh2                                 x86_64                          1.9.0-5.fc32                                  fedora                           119 k
 linux-atm-libs                          x86_64                          2.5.1-26.fc32                                 fedora                            38 k
 nmap-ncat                               x86_64                          2:7.80-4.fc32                                 updates                          236 k
 psmisc                                  x86_64                          23.3-3.fc32                                   fedora                           159 k
 vim-common                              x86_64                          2:8.2.2787-1.fc32                             updates                          6.7 M
 vim-filesystem                          noarch                          2:8.2.2787-1.fc32                             updates                           22 k
 which                                   x86_64                          2.21-19.fc32                                  fedora                            42 k
Installing weak dependencies:
 iproute-tc                              x86_64                          5.9.0-1.fc32                                  updates                          432 k
…
Installed:
  gpm-libs-1.20.7-21.fc32.x86_64           iproute-5.9.0-1.fc32.x86_64                iproute-tc-5.9.0-1.fc32.x86_64    iputils-20200821-1.fc32.x86_64        
  libssh2-1.9.0-5.fc32.x86_64              linux-atm-libs-2.5.1-26.fc32.x86_64        nmap-2:7.80-4.fc32.x86_64         nmap-ncat-2:7.80-4.fc32.x86_64        
  procps-ng-3.3.16-2.fc32.x86_64           psmisc-23.3-3.fc32.x86_64                  tree-1.8.0-4.fc32.x86_64          vim-common-2:8.2.2787-1.fc32.x86_64   
  vim-enhanced-2:8.2.2787-1.fc32.x86_64    vim-filesystem-2:8.2.2787-1.fc32.noarch    which-2.21-19.fc32.x86_64        

Complete!
[root@myfedora /]# 
```

```
[root@myfedora /]# nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 11:56 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000020s latency).
Other addresses for localhost (not scanned): ::1
All 1000 scanned ports on localhost (127.0.0.1) are closed
Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds

[root@myfedora /]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
19: eth0@if20: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

[root@myfedora /]# ping -c1 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=56 time=13.3 ms

--- 1.1.1.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 13.317/13.317/13.317/0.000 ms
```

Ara que el container té instal·lades totes les utilitats que ens agrada tenir en un sistema podem generar una imatge basada en aquest container. És com fer un snapshot o com gravar un DVD del container generant una imatge Read Only amb el nom que li vulguem donar. Per fer-ho utilitzem des d’una altra consola l’ordre **docker commit <container> <nom-nova-imatge>**. Quan es genera la imatge pot ser que el container si està en execució es posi uns segons en pause, i és que evidentment per fer una còpia del sistema de fitxers cal que no estigui actiu.

**Atenció**: en l’exemple següent per simplicitat li hem posat el mateix nom al container, al hostname i a la imatge, tots tres es diuen *myfedora*, però poden ser noms diferents.

```
$ docker ps
CONTAINER ID   IMAGE       COMMAND       CREATED          STATUS          PORTS     NAMES
42c204b8cb9c   fedora:32   "/bin/bash"   15 minutes ago   Up 15 minutes             myfedora
```

```
$ docker commit myfedora myfedora:latest
sha256:6c3b8d6fdfe5233d866eccee8d3836c375de3222d42677d6784c1dcf71cb1541
```

```
$ docker images myfedora:latest
REPOSITORY   TAG       IMAGE ID       CREATED          SIZE
myfedora     latest    6c3b8d6fdfe5   15 seconds ago   514MB
```

```
$ docker history myfedora:latest 
IMAGE          CREATED              CREATED BY                                      SIZE      COMMENT
6c3b8d6fdfe5   About a minute ago   /bin/bash                                       313MB     
c451de0d2441   14 months ago        /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B        
<missing>      14 months ago        /bin/sh -c #(nop) ADD file:2825a5543f1544841…   202MB     
<missing>      15 months ago        /bin/sh -c #(nop)  ENV DISTTAG=f32container …   0B        
<missing>      15 months ago        /bin/sh -c #(nop)  LABEL maintainer=Clement …   0B        
```

Podem observar de les taules anteriors que ara tenim localment una imatge anomenada myfedora i també en podem observar les capes que la conformen. Les capes inferiors són les originals de la imatge Fedora:32 i a sobre se li ha afegit la capa generada amb el docker commit (que li la instal·lat uns 313 MB en els paquets que hem triat). 

Ara disposem d’una imatge local anomenada myfedora:latest que és una fedora:32 amb els paquets que ens agraden. La podem usar com qualsevol altre imatge.

  * Primarament parem el container que hem usat coma model per generar la imatge. Aquest container s’eliminarà perquè hem posat el –rm.

  * Podem generar nous containers tal i com hem après en els primers capítols.

  * Procurem posar en tots ells l’opció --rm per evitar anar acumulant containers…

  * Amb un container interactiu activat si fem docker ps podem observar de quina imatge s’ha generat el container.

```
[root@myfedora /]# exit
exit

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

```
$ docker run --rm -it myfedora:latest /bin/bash
[root@f896eb227ec7 /]# ps ax
    PID TTY      STAT   TIME COMMAND
      1 pts/0    Ss     0:00 /bin/bash
     20 pts/0    R+     0:00 ps ax
[root@f896eb227ec7 /]# exit
exit


$ docker run --rm --name prova -h host.edt.org -it myfedora:latest cat /etc/hostname
host.edt.org

$ docker run --rm -it myfedora:latest ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
25: eth0@if26: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

```
$ docker run --rm -h host.edt.org -it myfedora:latest /bin/bash
[root@host /]#
```
```
$ docker ps
CONTAINER ID   IMAGE             COMMAND       CREATED          STATUS          PORTS     NAMES
e3e5d4002aa0   myfedora:latest   "/bin/bash"   19 seconds ago   Up 18 seconds             vigorous_easley
```


## Anomenar  imatges: TAG

Un cop hem creat imatges localment aquestes es poden pujar al repository al Cloud de DockerHub. Evidentment cal un compte al Dockerhub i veurem més endavant que el nom de la imatge ha de complir amb el següent requisit:

```
nomusuari/nomimatge:tag
```

Però anem a pams. Primerament anem a aprendre a posar noms a les imatges amb l’ordre **docker tag <nomactual> <nomnou>**.  Ens cal tenir en compte els següents aspectes a l’hora d’anomenar les imatges:

  * Igual que passa en català que la paraula nom pot ser un nom com “pere” o pot ser “pere puig”  o pot ser “pere pou prat”, en docker un nom o tag d’una imatge també té ambigüitat.

  * El nom o TAG d’una imatge es pot composar de les parts: usuari/nomimatge:tag.

    * **usuari** identifica a l’usuari i ha de correspondre amb el nom del compte al dockerhub si volem poder pujar la imatge al nostre repositori. Aquest primer element és opcional (però si no hi és no podrem pujar les imatges).
La part del nom de l’usuari i el nom de la imatge es separen per una barra vertical.

    * **nomimatge** és el nom que donem a la imatge, per exemple myfedora o ldapserver o el nom que volguem. Aquesta part és obligatòria.

    * **tag** aquest últim element és una etiqueta que acostuma a diferenciar versions diferents d’una mateixa imatge. Per exemple myfedora:v1 o myfedora:v2 o myfedora:latest. Observeu que el nom i el tag es separen per dos punts.
Quan a una imatge no se li indica específicament un tag aquest pren el valor latest. Així és el mateix dir myfedora a seques que dir myfedora:latest.

  * Observeu que la paraula TAg tant es fa servir per referir-se a tot el nom (amb el sentit d’etiquetar la imatge) com per referir-se a la última part (en el sentit de l’etiqueta que diferencia versions de la mateixa imatge).

Exemples de noms d’imatges:

 * myimage  (és el mateix que myimage:latest)
 * myimage:v1
 * myimage:latest
 * linux:prova
 * linux:produccio
 * linux:v2
 * linux:latest
 * pere/ldap:latest (és el mateix que pere/ldap)
 * pere/ldap:v1
 * pere/ldap:tls
 * edtasixm05/net21:base

Finalment anem a veure d’una vegada com posar noms a les imatges amb docker tag. Feu atenció que no es canvia el nom a una imatge sinó que se li afegeix un nou nom. Una imatge pot tenir varis noms però només és una imatge (s’identifica pel ID)

 * Assignar un segon nom a la imatge myfedora:latest, per exemple myfedora:v1.

 * Observeu que ara teniu dues imatges que en realitat són la mateixa (ID  6c3b8d6fdfe5 en l’exemple).  En realitat la frase està mal dita, hi ha una sola imatge amb dos noms diferents.

```
$ docker tag myfedora:latest myfedora:v1

$ docker images myfedora
myfedora         myfedora:latest  myfedora:v1      

$ docker images myfedora
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
myfedora     latest    6c3b8d6fdfe5   3 hours ago   514MB
myfedora     v1        6c3b8d6fdfe5   3 hours ago   514MB
```

  * Podem posar-li els noms o tags que volguem amb el format que considerem oportu. Anem a posar-li un nom sencer amb usuari, nom i tag.

```
$ docker tag myfedora:latest edtasixm05/linux:prova

$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
myfedora     latest    6c3b8d6fdfe5   3 hours ago   514MB
myfedora     v1        6c3b8d6fdfe5   3 hours ago   514MB
edtasixm05/linux   prova     6c3b8d6fdfe5   3 hours ago   514MB
```

  * **Atenció**:  una imatge amb més d’un nom no es pot esborrar per ID perquè no ho permet, cal eliminar-la tag a tag (nom a nom). Aquest és un típic error de principiant, voler esborra imatges i obtenir el missatge d’error. El que cal fer és eliminar-les per nom. **En cap cas forcem** l’eliminació de la imatge, és perjudicial.

```
$ docker rmi 6c3b8d6fdfe5
Error response from daemon: conflict: unable to delete 6c3b8d6fdfe5 (cannot be forced) - image is being used by running container e3e5d4002aa0
```

Per eliminar una imatge amb múltiples tag cal anar eliminant tag a tag. Quan s’esborra un tag però encara en queden més només fa el untag (elimina l’associació). Només quan és l’últim nom llavors si que elimina definitivament la imatge. **Nota**: no elimineu tots els noms de la imatge per poder-la pujar al repositori.

```
$ docker rmi myfedora:v1
Untagged: myfedora:v1

$ docker rmi edtasixm05/linux:prova 
Untagged: edtasixm05/linux:prova
 
$ docker images myfedora
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
myfedora     latest    6c3b8d6fdfe5   3 hours ago   514MB
```

Anem a veure un exemple on posem noms a una imatge oficial descarregada i l’acabem esborrant.

  * Primerament descarreguem alpine si no ho havíem fet.

```
$ docker pull alpine
Using default tag: latest
latest: Pulling from library/alpine
Digest: sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c
Status: Image is up to date for alpine:latest
docker.io/library/alpine:latest
```

  * Li posem un parell de tags inventats. Observem que si li posem un nom sense etiqueta final aquesta per defecte és latest.

```
$ docker tag alpine:latest  pebrot

$ docker tag alpine:latest  pebrot:v1

$ docker image
myfedora                      latest           6c3b8d6fdfe5   3 hours ago     514MB
alpine                        latest           e66264b98777   6 weeks ago     5.53MB
pebrot                        latest           e66264b98777   6 weeks ago     5.53MB
pebrot                        v1               e66264b98777   6 weeks ago     5.53MB
```

  * Observem que no es pot eliminar per ID perquè hi ha múltiples noms (tags) referenciant la imatge.

```
$ docker rmi e66264b98777
Error response from daemon: conflict: unable to delete e66264b98777 (must be forced) - image is referenced in multiple repositories
```

```
$ docker rmi alpine:latest 
Untagged: alpine:latest
Untagged: alpine@sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c

$ docker rmi pebrot:latest 
Untagged: pebrot:latest

$ docker rmi pebrot:v1
Untagged: pebrot:v1
Deleted: sha256:e66264b98777e12192600bf9b4d663655c98a090072e1bab49e233d7531d1294
```



### Dependències a l’hora d’esborrar imatges i containers

No es poden esborra imatges de les que hi ha containers creats, ens indicarà que hi ha un conflicte i que fins que no s’elimini el container no es pot eliminar la imatge.

Tampoc es poden eliminar imatges que són base d’altres imatges. Així per exemple la nostra imatge myfedora es basa en la imatge fedora:32, no ens deixarà esborrar fedora:32 localment perquè l’altra imatge en depèn (primer caldria esborra myfedora). 

  * Crear un  container basat en alpine i intentar eliminar la imatge alpine.

  * El container el creem sense l’opció - -rm de manera que en finalitzar continua existint.

```
$ docker run --name mycontainer -it alpine 
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
2408cc74d12b: Already exists 
Digest: sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c
Status: Downloaded newer image for alpine:latest
/ # hostname
a61be1c0799c
/ # exit

$ docker ps -a
CONTAINER ID   IMAGE                    COMMAND       CREATED              STATUS                      PORTS     NAMES
a61be1c0799c   alpine                   "/bin/sh"     About a minute ago   Exited (0) 54 seconds ago             mycontainer
```

  * Eliminar la imatge alpine. No es pot perquè indica que el container a61be1c0799c en fa referència. Just el container mycontainer.

```
$ docker rmi alpine:latest 
Error response from daemon: conflict: unable to remove repository reference "alpine:latest" (must force) - container a61be1c0799c is using its referenced image e66264b98777
```

  * Si s’elimina el container llavors si es pot eliminar la imatge.

```
$ docker rm mycontainer 
mycontainer

$ docker rmi alpine:latest 
Untagged: alpine:latest
Untagged: alpine@sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c
Deleted: sha256:e66264b98777e12192600bf9b4d663655c98a090072e1bab49e233d7531d1294
```


## Pujar imatges al Docker Hub

Per poder pujar imatges desenvolupades per nosaltres al DockerHub  cal evidentment disposar d’un compte d’usuari en aquest servei, però també cal que la imatge tingui un nom apropiat. 

El format del nom de les imatges ha de ser:
```
usuari/imatge:tag
```

Per tant quan es creen imatges que volem pujar al repositori és un bon hàbit ja posar-li el nom apropiat, per no haver de fer ordres docker tag i acabar tenint un munt de noms, que sempre acaba portant problemes.

```
docker commit <container> <user/image:tag>
```

```
docker build -t  <user/image:tag> .
```

Anem a pujar la imatge myfedora:latest al dockerhub

  * Primerament cal donar-li un nom apropiat. No li hem posat tag per tant ha assignat per defecte el tag latest. En l’exemple el nom d’usuari és edtasixm05.

```
$ docker tag myfedora:latest edtasixm05/myfedora

$ docker images edtasixm05/myfedora:latest 
REPOSITORY            TAG       IMAGE ID       CREATED       SIZE
edtasixm05/myfedora   latest    6c3b8d6fdfe5   4 hours ago   514MB
```

  * Per poder pujar contingut al DockerHub cal fer el login des de consola. Es disposa de les ordres **docker login** i **docker logout** per iniciar i tancar la sessió des de consola al dockerhub.

  * En fer el login es genera un fitxer amb les credencials al home de l’usuari. Les bones pràctiques de confidencialitat demanen que un cop acabem de treballar pensem a fer el docker logout i no deixar el fitxer amb les credencials al sistema (el logout l’elimina).

  * Per pujar les imatges s’utilitza l’ordre **docker push <imatge>**.

```
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: edtasixm05
Password: 
WARNING! Your password will be stored unencrypted in /home/ecanet/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

$ ls -l .docker/config.json 
-rw-------. 1 ecanet ecanet 103 Jul  5 17:49 .docker/config.json
```

```
$ docker push edtasixm05/myfedora:latest 
The push refers to repository [docker.io/edtasixm05/myfedora]
d1a0d51a9be5: Pushed 
fa96786f7d52: Mounted from library/fedora 
latest: digest: sha256:5416c06a7c1c8420851439f6670d4152077cbf9855157bf7e774c4aa37d6c152 size: 742
```

  * Podem pujar múltiples versions iguals o diferents de la imatge. Anem a posar-li un nou tag a la mateixa imatge i pujar-la també. Veurem que en realitat com que el contingut ja existeix ara no li cal pujar les capes, simplement en comprovar el seu hash ja detecta que estan pujades.

```
$ docker tag edtasixm05/myfedora:latest edtasixm05/myfedora:versio1

$ docker push edtasixm05/myfedora:versio1 
The push refers to repository [docker.io/edtasixm05/myfedora]
d1a0d51a9be5: Layer already exists 
fa96786f7d52: Layer already exists 
versio1: digest: sha256:5416c06a7c1c8420851439f6670d4152077cbf9855157bf7e774c4aa37d6c152 size: 742
```

  * Tal i com s’ha explicat abans no es pot pujar una imatge al repositori de l’usuari si no té el nom apropiat. En l’exemple no es pot pujar la imatge myfedora:latest perquè no inclou el nom de l’usuari.

```
$ docker push myfedora:latest 
The push refers to repository [docker.io/library/myfedora]
d1a0d51a9be5: Preparing 
fa96786f7d52: Preparing 
denied: requested access to the resource is denied
```

  * Si s’inicia una sessió amb el navegador al Dockerhub es pot veure la imatge pujada i en l’apartat tags els dos tags existents actualment.

![image1](images/image1.png)

```
```

![image2](images/image2.png)

```
```

![image3](images/image3.png)



  * Es recomana d’editar la descripció de les imatges i els tags generant una frase descriptiva i escrivint un petit text en markdown.

![image4](images/image4.png)


```
$ docker search edtasixm05
NAME                    DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
edtasixm05/web21        @edt Curs 2021-2022 ASIX-M01 Exemple bàsic d…   0                    
edtasixm05/net21        @edt Curs 2021-2022 ASIX-M01 Exemple bàsic d…   0                    
edtasixm05/getstarted   @edt Curs 2021-2022 ASIX-M01 Exemples de doc…   0                    
edtasixm05/mylinux      exemple a l'aula de crear un fedora amb ordr…   0                    
edtasixm05/myweb                                                        0                    
edtasixm05/ssh21        @edt Curs 2021-2022 ASIX-M01 Exemple bàsic d…   0                    
edtasixm05/postgres21   @edt Curs 2021-2022 ASIX-M05 Imatge postgres…   0                    
edtasixm05/myfedora     @edtasixm05 2022. Imatge d'exemple de Fedora…   0      
```


 * **Atenció**: recordeu sempre en finalitzar de treballar de fer el docker logout.

```
$ docker logout
Removing login credentials for https://index.docker.io/v1/
```

  * Anem a fer la prova de veritat. Eliminem localment totes les versions de la imatge, totes. I després comprovem que podem generar containers basats en la imatge pujada al dockerhub.

  * Observem que un cop eliminades les imatges si generem un container basat en la imatge del cloud aquesta s’ha de descarregar localment abans de poder crear el container i executar-lo.

```
$ docker rmi myfedora:latest edtasixm05/myfedora:latest edtasixm05/myfedora:versio1 
Untagged: myfedora:latest
Untagged: edtasixm05/myfedora:latest
Untagged: edtasixm05/myfedora:versio1
Untagged: edtasixm05/myfedora@sha256:5416c06a7c1c8420851439f6670d4152077cbf9855157bf7e774c4aa37d6c152
Deleted: sha256:6c3b8d6fdfe5233d866eccee8d3836c375de3222d42677d6784c1dcf71cb1541
Deleted: sha256:4f5eefa08b73de8f3e941c796eeb54d192a984d71be626f6670f12db6f48a566
Deleted: sha256:c451de0d24418278b3b8e66bdb6814f92ea1ac7764201c6e63c64c030071c7ba
Deleted: sha256:fa96786f7d5220b5ed1bc45d0998f6e059b91b374f16c59941db0cf4097b74d7
```

```
$ docker run --rm -it edtasixm05/myfedora date
Unable to find image 'edtasixm05/myfedora:latest' locally
latest: Pulling from edtasixm05/myfedora
de2fa1c557bb: Pull complete 
4340c1d50303: Pull complete 
Digest: sha256:5416c06a7c1c8420851439f6670d4152077cbf9855157bf7e774c4aa37d6c152
Status: Downloaded newer image for edtasixm05/myfedora:latest
Tue Jul  5 16:08:56 UTC 2022
```

  * **Atenció!** Tenir imatges que estem desenvolupant localment i la mateixa imatge al cloud pot generar problemes de que no estem treballant amb la versió de la imatge que ens pensem. És com quan depures i proves un programa i resulta que el fixer que estàs editant no és el mateix que el que estàs provant.



## Generar una imatge debian personalitzada amb Dockerfile

El mecanisme usual i recomanat per generar imatges en Docker és a través de la creació d’un **Dockerfile**. Es tracta d’un fitxer que conté la llista de tots els passos necessaris a fer per a la creació de la imatge. Utilitza un llenguatge propi però és similar a una recepta o un script.

Aquest mecanisme és el preferit perquè amb un simple fitxer de text tenim la ‘recepta’ de com crear la imatge. Potser la imatge ocupa varis MegaBytes però el fitxer és un simple fitxeret de text de pocs bytes. Usualment es desen al Git els fitxers Dockerfile que creen les imatges dissenyades per els usuaris.

Anem a fer la pràctica de crear una imatge basada en la última versió de debian i hi afegirem els paquets necessaris per poder fer ordres típiques del sistema operatiu GNU/Linux que no estan instal·lades per defecte:

  * **procps** paquet per permetre fer ordres ps.
  * **iproute2** paquet per poder fer ordres ip.
  * **iputils-ping** paquet per poder fer pings.
  * **nmap** paquet per fer nmap.
  * **tree** paquet de l’ordre tree.
  * **vim** per disposar de l’editor.

Cal crear un directori de treball (directori de context) i dins seu crear el fitxer Dockerfile i tot allò necessari per crear la imatge.

Exemple de fitxer Dockerfile

```
#  Versió de debian personalitzada amb paquets bàsics
FROM debian:latest
LABEL author="@edt ASIX Curs 2022"
LABEL curs="docker / AWS"
RUN apt-get update && apt-get -y install procps iproute2 iputils-ping nmap tree vim
WORKDIR /tmp
CMD /bin/bash
```

Per generar imatges basades en fitxers Dockerfile s’utilitza l’ordre docker build amb els arguments:

 * **-t** imagename nom que tindrà la imatge creada.

 * **.** (un punt) és el directori de context.

És important entendre el concepte del punt. El punt indica el directori actual que és passat com a directori de context. Normalment es desenvolupa una imatge en un directori propi que conté el Dockerfile i tot allò necessari per generar la imatge. El nom pot ser diferent però mai es fa servir un nom que no sigui Dockerfile (amb la D majúscula). 

```
docker build -t <imagename> .
```

  * Així, dins del directori de context generem amb docker build la imatge.

```
$ pwd
/var/tmp/m05/docker/dockefiles/mydebian

$ ls 
Dockerfile
```

```
$ docker build -t edtasixm05/mydebian .
Sending build context to Docker daemon  2.048kB
Step 1/6 : FROM debian:latest
 ---> 82bd5ee7b1c5
Step 2/6 : LABEL author="@edt ASIX Curs 2022"
 ---> Using cache
 ---> bbff987a7df3
Step 3/6 : LABEL curs="docker / AWS"
 ---> Using cache
 ---> 45ad1f10245d
Step 4/6 : RUN apt-get update && apt-get -y install procps iproute2 iputils-ping nmap tree vim
 ---> Running in 6cc66ca2a347
Get:1 http://security.debian.org/debian-security bullseye-security InRelease [44.1 kB]
Get:2 http://deb.debian.org/debian bullseye InRelease [116 kB]
Get:3 http://deb.debian.org/debian bullseye-updates InRelease [39.4 kB]
Get:4 http://security.debian.org/debian-security bullseye-security/main amd64 Packages [164 kB]
Get:5 http://deb.debian.org/debian bullseye/main amd64 Packages [8182 kB]
Get:6 http://deb.debian.org/debian bullseye-updates/main amd64 Packages [2592 B]
Fetched 8548 kB in 2s (4999 kB/s)
Reading package lists...
…
Processing triggers for libc-bin (2.31-13) ...
Removing intermediate container 6cc66ca2a347
 ---> 77a8c0fe5056
Step 5/6 : WORKDIR /tmp
 ---> Running in 1a24087b54fc
Removing intermediate container 1a24087b54fc
 ---> 461abf213399
Step 6/6 : CMD /bin/bash
 ---> Running in 8648e23e6e7d
Removing intermediate container 8648e23e6e7d
 ---> 19dfe998035f
Successfully built 19dfe998035f
Successfully tagged edtasixm05/mydebian:latest
```

  * Cal fixar-se que en el procés docker build ha generat una capa (una imatge intermitja) per a cada instrucció del Dockerfile. 

  * També cal comprovar que en acabar indica Successfully, si no ho diu no s’ha generat la imatge.

  * Si en la creació d’una imatge es produeixen errors quan es rectifiquen i es genera de nou no cal tornar a fer tots els passos sinó des del pas que ha fallat fins el final. Per això cada pas genera una imatge intermitja.

  * La imatge s’ha creat i existeix localment. Com que en el nom no se li ha posat tag ha afagat latest per defcte..

```
$ docker images edtasixm05/mydebian
REPOSITORY            TAG       IMAGE ID       CREATED         SIZE
edtasixm05/mydebian   latest    19dfe998035f   3 minutes ago   217MB


Es poden veure les capes de la nova imatge que coincideixen amb les de el debian:latest originali s’hi han afegit les generades amb el Dockerfile.

$ docker history edtasixm05/mydebian:latest 
IMAGE          CREATED         CREATED BY                                      SIZE      COMMENT
19dfe998035f   4 minutes ago   /bin/sh -c #(nop)  CMD ["/bin/sh" "-c" "/bin…   0B        
461abf213399   4 minutes ago   /bin/sh -c #(nop) WORKDIR /tmp                  0B        
77a8c0fe5056   4 minutes ago   /bin/sh -c apt-get update && apt-get -y inst…   92.8MB    
45ad1f10245d   5 minutes ago   /bin/sh -c #(nop)  LABEL curs=docker / AWS      0B        
bbff987a7df3   5 minutes ago   /bin/sh -c #(nop)  LABEL author=@edt ASIX Cu…   0B        
82bd5ee7b1c5   10 months ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      10 months ago   /bin/sh -c #(nop) ADD file:1fedf68870782f1b4…   124MB     
```

  * Ara podem provar la imatge generant un container i observar que funciona.

```
$ docker run --rm -it edtasixm05/mydebian ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
40: eth0@if41: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

  * També podem pujar la imatge al repository al Cloud de DockerHub. Ho farem usant dos tags, el de latest i el de v1.

  * Recordeu que cal tenir les credentials de connexió. Si ens hem desconnectat cal tornar a fer l’ordre docker login.

```
$ docker tag edtasixm05/mydebian:latest edtasixm05/mydebian:v1

$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: edtasixm05
Password: 
WARNING! Your password will be stored unencrypted in /home/ecanet/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

$ docker push edtasixm05/mydebian:latest 
The push refers to repository [docker.io/edtasixm05/mydebian]
97d574848297: Pushed 
799760671c38: Mounted from edtasixm05/net21 
latest: digest: sha256:68d9b8a7cee00d018f2e75ecc7213fcaf3aa5a9ba22b75102f048a149c2998b1 size: 741

$ docker push edtasixm05/mydebian:v1
The push refers to repository [docker.io/edtasixm05/mydebian]
97d574848297: Layer already exists 
799760671c38: Layer already exists 
v1: digest: sha256:68d9b8a7cee00d018f2e75ecc7213fcaf3aa5a9ba22b75102f048a149c2998b1 size: 741
```

  * Tornem a fer la prova definitiva. Eliminem les imatges locals i verifiquem que podem generar containers basats en aquesta imatge, usant qualsevol dels dos tags.


  * Observeu que en generar el primer container li cal descarregar la imatge del repository, però per al segon no.

```
$ docker rmi edtasixm05/mydebian:latest edtasixm05/mydebian:v1 
Untagged: edtasixm05/mydebian:latest
Untagged: edtasixm05/mydebian:v1
Untagged: edtasixm05/mydebian@sha256:68d9b8a7cee00d018f2e75ecc7213fcaf3aa5a9ba22b75102f048a149c2998b1
Deleted: sha256:19dfe998035f77ef9c8edbd8ffc84bcd4432da84b607c2c19a653223c9d8a62b
Deleted: sha256:461abf2133991127e5afc6f8e887b89cd7b90a65cf6c34717219a37acae4a20b
Deleted: sha256:77a8c0fe50564b97ba425cd224b3cec23b11e9bb200d666a8c240c09530ef322
Deleted: sha256:e57f60fca3b1f4b5ee26950d7d525461ff694126e2578de91fd72b5e3d5a08f6
```

```
$ docker run --rm -it edtasixm05/mydebian:latest date
Unable to find image 'edtasixm05/mydebian:latest' locally
latest: Pulling from edtasixm05/mydebian
955615a668ce: Already exists 
c2ca4ebcce04: Pull complete 
Digest: sha256:68d9b8a7cee00d018f2e75ecc7213fcaf3aa5a9ba22b75102f048a149c2998b1
Status: Downloaded newer image for edtasixm05/mydebian:latest
Tue Jul  5 16:56:07 UTC 2022

$ docker run --rm -it edtasixm05/mydebian:v1 ps ax
Unable to find image 'edtasixm05/mydebian:v1' locally
v1: Pulling from edtasixm05/mydebian
Digest: sha256:68d9b8a7cee00d018f2e75ecc7213fcaf3aa5a9ba22b75102f048a149c2998b1
Status: Downloaded newer image for edtasixm05/mydebian:v1
    PID TTY      STAT   TIME COMMAND
      1 pts/0    Rs+    0:00 ps ax
```

 * Observem finalment que al Docker Hub tenim la imatge pujada amb els dos tags i li afegim una descripció.

![image5](images/image5.png)


## Fem una segona versió de la imatge

Anem a fer una segona versió de la imatge on posarem en un directory anomenat /normativa dos fitxers, un amb la normativa del centre (un pdf) i un altre amb el funcionament de les aules (un markdown). 

 * Primerament generem dos fitxers en el directori de context anomenats normes.pdf i aules.md. Podeu enerar-los com volgueu!. Aquí farem trampeta i els simularem.

```
$ pwd
/var/tmp/m05/docker/dockefiles/mydebian

$ echo "normes de l'aula: ser nets i endreçats" > aules.md

$ echo "normes del centre: fer cas als profes" > normes.pdf

$ ls
aules.md  Dockerfile  normes.pdf
```

  * Modifiquem el fitxer Dockerfile per crear el directori i copiar-hi els fitxers. Observeu que en copiar múltiples fitxers a un directori destí la ruta ha d’acabar amb la barra.

  * **Atenció**: cal interpretar el sentit de l’ordre COPY (i l’ordre ADD, un copy amb asteroides).  L’origen són els fitxers que estan al directori de context, es tracta d’una ruta relativa sempre dins del directori de context. El destí és una ruta dins de la imatge, sempre dins de la imatge. Tenim dues realitats, la de fora i la de dins de la imatge.

```
# Versió de debian personalitzada amb paquets bàsics
FROM debian:latest
LABEL author="@edt ASIX Curs 2022"
LABEL curs="docker / AWS"
RUN apt-get update && apt-get -y install procps iproute2 iputils-ping nmap tree vim
RUN mkdir /normativa
COPY aules.md normes.pdf /normativa/
WORKDIR /tmp
CMD /bin/bash
```

  * Generem una nova imatge anomenada edtasixm05/mydebian:v2. Assegurem-nos que al final de tot indica Successfully.

```
$ docker build -t edtasixm05:mydebian:v2 .
invalid argument "edtasixm05:mydebian:v2" for "-t, --tag" flag: invalid reference format
See 'docker build --help'.
$ docker build -t edtasixm05/mydebian:v2 .
Sending build context to Docker daemon  4.096kB
Step 1/8 : FROM debian:latest
 ---> 82bd5ee7b1c5
Step 2/8 : LABEL author="@edt ASIX Curs 2022"
 ---> Using cache
 ---> bbff987a7df3
Step 3/8 : LABEL curs="docker / AWS"
 ---> Using cache
 ---> 45ad1f10245d
Step 4/8 : RUN apt-get update && apt-get -y install procps iproute2 iputils-ping nmap tree vim
 ---> Running in 9e4c9f20fca2
Get:1 http://deb.debian.org/debian bullseye InRelease [116 kB]
Get:2 http://security.debian.org/debian-security bullseye-security InRelease [44.1 kB]
Get:3 http://deb.debian.org/debian bullseye-updates InRelease [39.4 kB]
Get:4 http://security.debian.org/debian-security bullseye-security/main amd64 Packages [164 kB]
Get:5 http://deb.debian.org/debian bullseye/main amd64 Packages [8182 kB]
Get:6 http://deb.debian.org/debian bullseye-updates/main amd64 Packages [2592 B]
Fetched 8548 kB in 3s (3366 kB/s)
Reading package lists...
…
Processing triggers for libc-bin (2.31-13) ...
Removing intermediate container 9e4c9f20fca2
 ---> 667b6a1bcd4c
Step 5/8 : RUN mkdir /normativa
 ---> Running in 75e6260fd74a
Removing intermediate container 75e6260fd74a
 ---> 7f4a9358d446
Step 6/8 : COPY aules.md normes.pdf /normativa/
 ---> acf009c423eb
Step 7/8 : WORKDIR /tmp
 ---> Running in d092357b50f2
Removing intermediate container d092357b50f2
 ---> ebc7b881cc6f
Step 8/8 : CMD /bin/bash
 ---> Running in eae5499a9e11
Removing intermediate container eae5499a9e11
 ---> 3ac951c9cb4e
Successfully built 3ac951c9cb4e
Successfully tagged edtasixm05/mydebian:v2
```

  * Provem que funciona la nova imatge generant un container que mostri la normativa, per exemple.

```
$ docker run --rm edtasixm05/mydebian:v2 ls /normativa
aules.md
normes.pdf

$ docker run --rm -it edtasixm05/mydebian:v2 /bin/bash
root@e54316e5bec7:/tmp# cat /normativa/aules.md 
normes de l'aula: ser nets i endreçats
root@e54316e5bec7:/tmp# exit
exit
```

  * Pujem la imatge al repository del DockerHub. Però ja que és la segona versió la pujem com a v2 però també com a latest.

  * Per tant li posarem aquest tag i en fer-ho es perdrà l’associació que tenia a la imatge v1 i passarà estar associat a la imatge v2.

  * Pujem tant la latest com la v2. En l’exemple s’ha pujat primer v2 i en pujar la latest ha indicat que les capes ja existien en el repositori.

```
$ docker push edtasixm05/mydebian:v2
The push refers to repository [docker.io/edtasixm05/mydebian]
14f858cb0dc3: Pushed 
5228f7ce4cf5: Pushed 
4ef05c745648: Pushed 
799760671c38: Layer already exists 
v2: digest: sha256:b30dafa8a14ff037fb171ac6c2c0b174c2e21d1aa2e9e1646521903f8a72184c size: 1155
```

```
$ docker tag edtasixm05/mydebian:v2 edtasixm05/mydebian:latest 

$ docker images edtasixm05/mydebian
REPOSITORY            TAG       IMAGE ID       CREATED          SIZE
edtasixm05/mydebian   latest    3ac951c9cb4e   7 minutes ago    217MB
edtasixm05/mydebian   v2        3ac951c9cb4e   7 minutes ago    217MB
edtasixm05/mydebian   v1        19dfe998035f   33 minutes ago   217MB
```

```
$ docker push edtasixm05/mydebian:latest 
The push refers to repository [docker.io/edtasixm05/mydebian]
14f858cb0dc3: Layer already exists 
5228f7ce4cf5: Layer already exists 
4ef05c745648: Layer already exists 
799760671c38: Layer already exists 
latest: digest: sha256:b30dafa8a14ff037fb171ac6c2c0b174c2e21d1aa2e9e1646521903f8a72184c size: 1155
```

  * Tal i com hem estat fent tota l’estona, esborrem les imatges locals i comprovem que funcionen les imatges del repository.

  * Per eliminar les imatges locals caldrà indicar el nom de la imatge perquè el ID té associats dos noms.

```
$ docker rmi edtasixm05/mydebian:latest edtasixm05/mydebian:v2 edtasixm05/mydebian:v1
Untagged: edtasixm05/mydebian:latest
Untagged: edtasixm05/mydebian:v2
Untagged: edtasixm05/mydebian@sha256:b30dafa8a14ff037fb171ac6c2c0b174c2e21d1aa2e9e1646521903f8a72184c
Deleted: sha256:3ac951c9cb4e98abf122d337ea1024a1ebfed8a07eb58cdffd3f03031ec88798
Deleted: sha256:ebc7b881cc6f24635eb596092f5053cdc9cb22837b0eeef999f9bef6e4ab4bf0
Deleted: sha256:acf009c423eb9c4229bf44a8ec0824d22f13b88a32733b4ced331c1b14ac2d0d
Deleted: sha256:1e422157466d5000b5e82fe36a47be800efa6ff7e37234ba562c80931f762cd1
Deleted: sha256:7f4a9358d446de5e0a802f0597400bb4247dd529d7699e68c2e32e3eff2960f5
Deleted: sha256:ccb3e73b79eb2f248bc0b36a22db98860c64e3930dd6fe40c3095e35b6dc9f65
Deleted: sha256:667b6a1bcd4cea6d613aea7d5c8f53e1eba2b6701fb2e28b238171470a0f520e
Deleted: sha256:3676b41a0191832b1a4a152bbfb75fa4f84aa07d080018c9e4aa2dbd84f6ff91
Untagged: edtasixm05/mydebian:v1
Untagged: edtasixm05/mydebian@sha256:68d9b8a7cee00d018f2e75ecc7213fcaf3aa5a9ba22b75102f048a149c2998b1
Deleted: sha256:19dfe998035f77ef9c8edbd8ffc84bcd4432da84b607c2c19a653223c9d8a62b
Deleted: sha256:e57f60fca3b1f4b5ee26950d7d525461ff694126e2578de91fd72b5e3d5a08f6
```

```
$ docker run --rm edtasixm05/mydebian:v1 date
Unable to find image 'edtasixm05/mydebian:v1' locally
v1: Pulling from edtasixm05/mydebian
955615a668ce: Already exists 
c2ca4ebcce04: Pull complete 
Digest: sha256:68d9b8a7cee00d018f2e75ecc7213fcaf3aa5a9ba22b75102f048a149c2998b1
Status: Downloaded newer image for edtasixm05/mydebian:v1
Tue Jul  5 17:20:19 UTC 2022

$ docker run --rm edtasixm05/mydebian ls /normativa
Unable to find image 'edtasixm05/mydebian:latest' locally
latest: Pulling from edtasixm05/mydebian
955615a668ce: Already exists 
fddf1907183c: Pull complete 
130132cae95b: Pull complete 
63e6cdfa474f: Pull complete 
Digest: sha256:b30dafa8a14ff037fb171ac6c2c0b174c2e21d1aa2e9e1646521903f8a72184c
Status: Downloaded newer image for edtasixm05/mydebian:latest
aules.md
normes.pdf
```

**Atenció**: en crear imatges a vegades es generen errors i llavors les imatges queden sense nom **<none>**. També ens pot passar que un cop pujades al repository si ja no ens calgui la imatge localment. Per tant cal anar fent repàs de les imatges que tenim localment i anar-les eliminant.

```
docker images

docker rmi <imatge> <imatge> …
```



