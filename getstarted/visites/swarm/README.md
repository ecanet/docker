# getting started - Comptador - Swarm 
## @edt ASIX-M05 Curs 2021-2022


### Exemple d'aplicació en Swarm


**Aplicació**

Crear un servei web amb Flask amb un comptador de visites Redis. Mostra el nom
del host (en aquest cas el container) que es visita.

  * Pàgina web amb Flask
  * Comptador de visites Redis
  * Persistència del comptador amb volum

**Swarm**

Activar Swarm en un node (o més...) i observar el desplegament de l'aplicació,
les rèpliques del container web i les restriccions de colocació.


#### Activar Docker Swarm en un o més nodes


Activar Swarm. Si cal indicar per quina interfície es publica que aquest és el node master.
Observar que en la resposta indica clarament que han de fer altres nodes si es volen unir al swarm.

```
$ docker swarm init
Error response from daemon: could not choose an IP address to advertise since this system has multiple addresses on different interfaces (192.168.1.105 on enp1s0 and 192.168.1.45 on wlp0s20f3) - specify one with --advertise-addr

$ docker swarm init --advertise-addr 192.168.1.105
Swarm initialized: current node (tkyzvoentsa2js2hmwqouk35m) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-5r6svnotfhim2xwslpovs4i9y51uhptidjx0dd4as7alam2ugp-6cx9w9w0mv1odbi8dz4lhruc4 192.168.1.105:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

```
$ docker node ls
ID                            HOSTNAME           STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
tkyzvoentsa2js2hmwqouk35m *   mylaptop.edt.org   Ready     Active         Leader           20.10.6

$ docker node 
demote   inspect  ls       promote  ps       rm       update   
```


#### Desplegar l'aplicació amb Docker Stack


Només desplegar els containers web i observar les rèpliques. No hi ha
comptador. Cal veure que en refrescar la pàgina pot variar el nom de host
(de container) que respón. Atenció amb el navegador, forçar IP:80.

```
$ docker stack deploy  -c docker-compose.yml myapp
Creating network myapp_webnet
Creating service myapp_web
```

```
$ docker stack 
deploy    ls        ps        rm        services  

$ docker stack ls
NAME      SERVICES   ORCHESTRATOR
myapp     1          Swarm

$ docker stack services myapp
ID             NAME        MODE         REPLICAS   IMAGE                         PORTS
kb9a27uk0ngd   myapp_web   replicated   5/5        edtasixm11/getstarted:part2   *:80->80/tcp
$ 
$ docker stack ps myapp
ID             NAME          IMAGE                         NODE               DESIRED STATE   CURRENT STATE                ERROR     PORTS
u0ke4wzo8hww   myapp_web.1   edtasixm11/getstarted:part2   mylaptop.edt.org   Running         Running about a minute ago             
mn3vhxxyl2ap   myapp_web.2   edtasixm11/getstarted:part2   mylaptop.edt.org   Running         Running about a minute ago             
sffr2cmecf21   myapp_web.3   edtasixm11/getstarted:part2   mylaptop.edt.org   Running         Running about a minute ago             
q5m801tbfh8x   myapp_web.4   edtasixm11/getstarted:part2   mylaptop.edt.org   Running         Running about a minute ago             
tchhgqyp33gb   myapp_web.5   edtasixm11/getstarted:part2   mylaptop.edt.org   Running         Running about a minute ago   
```

```
Connectar amb el navegador al port 80, atenció al navegador,
forçar 127.0.0.1:80 o la IP:80...
```

```
$ docker stack down myapp
Removing service myapp_web
Removing network myapp_webnet
```


#### Desplegar-la tota sencera

Ara es deplega tota l'aplicació amb el Python/Flask+Redis+Visualizer i podem observar que
hi ha un node al swarm i els containers i rèpliques que s'estan executant. Accedir amb el 
navegador al port 80 i refrescar per incrementar el comptador de visites, accedir al port
8080 per veure el visualizer. Atenció al navegador: IP:port.


```
$ docker stack deploy  -c docker-compose-pas4-visualizer.yml myapp
Creating network myapp_webnet
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_web

$ docker stack ps myapp
ID             NAME                 IMAGE                             NODE               DESIRED STATE   CURRENT STATE                ERROR     PORTS
4iwp6t554o8l   myapp_redis.1        redis:latest                      mylaptop.edt.org   Running         Running about a minute ago     
isrg1fylbaiu   myapp_visualizer.1   dockersamples/visualizer:stable   mylaptop.edt.org   Running         Running about a minute ago    
0kfosq1eydfa   myapp_web.1          edtasixm11/getstarted:part2       mylaptop.edt.org   Running         Running about a minute ago     
7epxj63o24qo   myapp_web.2          edtasixm11/getstarted:part2       mylaptop.edt.org   Running         Running about a minute ago     
x9dy9btz0oir   myapp_web.3          edtasixm11/getstarted:part2       mylaptop.edt.org   Running         Running about a minute ago     
ogv5u7kukdyb   myapp_web.4          edtasixm11/getstarted:part2       mylaptop.edt.org   Running         Running about a minute ago     
nqn9q84hq5t6   myapp_web.5          edtasixm11/getstarted:part2       mylaptop.edt.org   Running         Running about a minute ago 

$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
j4bcm817gbgx   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
pq7yhfm5xb0m   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
voh1pmknfbjh   myapp_web          replicated   5/5        edtasixm11/getstarted:part2       *:80->80/tcp
```

```
Connectar al viaualizer port 8080
Atenció a la IP a usar, forçar 127.0.0.1 o la de publicació...
```

```
$ docker stack rm myapp
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

#### Eliminar el Swarm 

```
$ docker swarm 
ca          init        join        join-token  leave       unlock      unlock-key  update     

$ docker swarm leave 
Error response from daemon: You are attempting to leave the swarm on a node that is participating as a manager. Removing the last manager erases all current state of the swarm. Use `--force` to ignore this message.

$ docker swarm leave --force
Node left the swarm.
```

