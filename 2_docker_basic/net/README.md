# net
## @edt ASIX-M05 Curs 2021-2022

# Networks

Per defecte Docker crea un xarxa anomenada **bridge** i desplega els containers dins d’aquesta xarxa. En el host amfitrió es genera la interfície virtual docker0 que té assignada sempre l’adreça 172.17.0.1/16. Aquesta xarxa default té assignat el rang 172.17.0.0/16 i els containers que es creen van reben adreces IP dinamicament dins d’aquest rang.

El primer que es crea és el 172.17.0.2/16, el següent el 172.17.0.3, etc. Però les adreces es reutilitzen, si apagem el primer container i n’engeguem un quart aquest agafarà la primera adreça lliure, la 172.17.0.2/16. Per tant hi ha l’inconvenient de que no sabem els containers quina adreça IP tindran.

A part de la xarxa default hi ha els següents altres tipus de xarxa:

  * **bridge** xarxa per defecte 172.170.0./16. No es fa resolució de noms de host dels container.

  * **host** xarxa exclusiva entre el container i el host amfitrió. Comparteixen xarxa i tenen comunicació entre ells dos, però el container no té comunicació a altres containers.

  * **none** el container no té interfície de xarxa.

Les xarxes en Docker són objectes propis, com els containers, imatges i volums i es gestionen amb l’ordre **docker network**.

La creació de xarxes personalitzades permet segregar els desplegaments en xarxes diferents de manera que esls containers que estan en xarxes diferents no tenen connectivitat entre ells, les xarxes estan isolades.


## Xarxa default

Les característiques de la xarxa **bridge** és que és la xarxa que s’utilitza si no s’indica el contrari, té el rang **172.17.0.0/16** i crea la interfície **docker0** en el host amfitrió. Aquest sempre t'l’adreça IP 172.17.0.1/16 i als containers se’ls van assignant sempre adreçes d’aquest rang dinàmicament, però reutilitzant-les.

Problemes:
  * No podem saber un container quina adreça té perque si els engeguem i parem les adreces deixen d’estar en ordre.
  * No es fa resolució de noms de container, no es pot accedir a uns i altres pel seu nom.
 
Es per això que la xarxa default, tot iser la que hem estat fent servir en la major part del dossier, no es fa servei pràcticament mai. Generalment els desplegaments generen la seva propia xarxa tal i com veurem més endavant.

Anem  a desplegar dos containers en la xarxa default i observar que tenen connectivitat per adreça IP però no per nom de host. Usarem la imatge edtasixm05/ssh21 que incorpora un servidor SSH (amb usuaris pere, marta i anna) i ordres om ping, ip a i nmap per observar les característiques de la xarxa.

  * Observem que al host amfitrió tenim la interfície docker0 i la seva adreça IP és la 172.17.0.1/16

```
$ ip a s dev docker0
11: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:7a:44:cc:17 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:7aff:fe44:cc17/64 scope link 
       valid_lft forever preferred_lft forever
```

  * Egeguem el servidor ssh. Observem:
    * La seva interfície de xarxa.
    * La seva adreça IP 172.17.0.2.
    * Que pot fer ping a l’adreça Ip del host amfitrió.
    * També pot fer ping a ell mateix.
    * També a l’exterior.
    * Mostrem la seva taula d’enrutament, on podem veure que el seu gateway és precisament el host amfitrió.

```
$ docker run --rm --name ssh.edt.org -h ssh.edt.org -d edtasixm05/ssh21 

$ docker run --rm --name ssh.edt.org -h ssh.edt.org -d edtasixm05/ssh21 
0f463ba15656c0ace035bbc61d21203e8c7ef39dcb54fb9909c6cba46e57d03b

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS     NAMES
0f463ba15656   edtasixm05/ssh21   "/bin/sh -c '/etc/in…"   3 seconds ago   Up 2 seconds   80/tcp    ssh.edt.org
 
$ docker exec -it ssh.edt.org ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
121: eth0@if122: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

$ docker exec -it ssh.edt.org ping -c1 172.17.0.1
PING 172.17.0.1 (172.17.0.1) 56(84) bytes of data.
64 bytes from 172.17.0.1: icmp_seq=1 ttl=64 time=0.094 ms

$ docker exec -it ssh.edt.org ping -c1 172.17.0.2
PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
64 bytes from 172.17.0.2: icmp_seq=1 ttl=64 time=0.023 ms

$ docker exec -it ssh.edt.org ping -c1 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=56 time=171 ms

$ docker exec -it ssh.edt.org ip r
default via 172.17.0.1 dev eth0 
172.17.0.0/16 dev eth0 proto kernel scope link src 172.17.0.2 
```

  * Engeguem un container que farà la funció de host client. En realitat engeguem la mateixa imatge del servidor SSH però en interactiu (sense el servei) per fer ús de les seves ordres de xarxa. Observem que:
    * La seva adreça IP és la següent la 172.17.0.3/16.
    * Pot fer ping al host amfitrió i al servidor ssh i a ell mateix.
    * Mostrem la seva taula d’enrutament on evidentment el host amfitriò és el seu gateway.

```
$ docker run --rm --name host1.edt.org -h host1.edt.org -it edtasixm05/ssh21  /bin/bash

root@host1:/tmp# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
123: eth0@if124: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

root@host1:/tmp# ping -c1 172.17.0.1
PING 172.17.0.1 (172.17.0.1) 56(84) bytes of data.
64 bytes from 172.17.0.1: icmp_seq=1 ttl=64 time=0.176 ms

--- 172.17.0.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.176/0.176/0.176/0.000 ms

root@host1:/tmp# ping -c1 172.17.0.2
PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
64 bytes from 172.17.0.2: icmp_seq=1 ttl=64 time=0.191 ms

--- 172.17.0.2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.191/0.191/0.191/0.000 ms


root@host1:/tmp# ping -c1 172.17.0.3
PING 172.17.0.3 (172.17.0.3) 56(84) bytes of data.
64 bytes from 172.17.0.3: icmp_seq=1 ttl=64 time=0.042 ms

--- 172.17.0.3 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.042/0.042/0.042/0.000 ms

root@host1:/tmp# ip r
default via 172.17.0.1 dev eth0 
172.17.0.0/16 dev eth0 proto kernel scope link src 172.17.0.3 
```

  * Però es pot connectar per SSH o fer ping al servidor SSH per el seu nom de host? No perquè la xarxa default no fa resolució de noms de container.

```
root@host1:/tmp# ssh pere@ssh.edt.org
ssh: Could not resolve hostname ssh.edt.org: Name or service not known

root@host1:/tmp# ping ssh.edt.org
ping: ssh.edt.org: Name or service not known
```

  * Però si que ho pot fer per adreça IP. Observer que es connecta com a pere password pere al servidor ssh.edt.org. Sortim i apaguem tant el client com el servidor SSH.

```
root@host1:/tmp# ssh pere@172.17.0.2
The authenticity of host '172.17.0.2 (172.17.0.2)' can't be established.
ECDSA key fingerprint is SHA256:fWJSkoIkntn1ViVsYywFj0imWTIBKNmEeHcE8hT1k7c.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.17.0.2' (ECDSA) to the list of known hosts.
pere@172.17.0.2's password: pere
Linux ssh.edt.org 5.11.22-100.fc32.x86_64 #1 SMP Wed May 19 18:58:25 UTC 2021 x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.

$ hostname
ssh.edt.org

$ exit
Connection to 172.17.0.2 closed.

root@host1:/tmp# exit
exit
```

```
$ docker stop ssh.edt.org 
ssh.edt.org
```

### Gestionar xarxes amb docker network

Docker proporciona l’ordre docker network per crear  i personalitzar xarxes. Es poden crear amb rangs per defcete o seleccionar el rang a crear. També es poden llistar, eliminar, consultar la configuració JSON, connectar i desconnectar a containers i eliminar les que no s’estiguin usant.

Per defecte si no s’indica el rang les crea correlativament. Si es vol crear usant un rang concret cal indicar amb subnet el rang i també amb gateway l’adreça IP del gateway. 

```
$ docker network 
connect     create      disconnect  inspect     ls          prune       rm         
```

```
$ docker network ls
NETWORK ID     NAME              DRIVER    SCOPE
53961f344b90   bridge            bridge    local
d63af1f95612   host              host      local
b98db7fe56a5   none              null      local
```

```
$ docker network create net1
a579f0f0ce8cf6730ae3211917971db4eef711d2441e79ef64a65a6589e1e4fb

$ docker network create net2
a353596d7a6d8baa420c1a16215be5d7aabd2f8ce8c376bf4f2ee51ad9599f0b

$ docker network ls
NETWORK ID     NAME              DRIVER    SCOPE
53961f344b90   bridge            bridge    local
a6e5e764e1a8   docker_gwbridge   bridge    local
d63af1f95612   host              host      local
a579f0f0ce8c   net1              bridge    local
a353596d7a6d   net2              bridge    local
b98db7fe56a5   none              null      local
```

```
$ docker network inspect net1 
[
    {
        "Name": "net1",
        "Id": "a579f0f0ce8cf6730ae3211917971db4eef711d2441e79ef64a65a6589e1e4fb",
        "Created": "2022-07-07T00:12:51.710996016+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]
```

```
$ docker network rm net1
net1

$ docker network prune 
WARNING! This will remove all custom networks not used by at least one container.
Are you sure you want to continue? [y/N] y
Deleted Networks:
net2
```


  * Crear xarxes amb el rang d’adreces IP personalitzat.

```
$ docker network create --
--attachable   --config-from  --driver       --help         --internal     --ipam-opt     --ipv6         --opt          --subnet
--aux-address  --config-only  --gateway      --ingress      --ipam-driver  --ip-range     --label        --scope        
```

```
$ docker network create --subnet 172.25.0.0/24 --gateway 172.25.0.1 net25
6da6e75ceb17e0c7e887cfaf54baba4bcbf59012270978e1b208d4147b161912
```

```
$ docker network inspect net25
[
    {
        "Name": "net25",
        "Id": "6da6e75ceb17e0c7e887cfaf54baba4bcbf59012270978e1b208d4147b161912",
        "Created": "2022-07-07T00:16:03.345648869+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.25.0.0/24",
                    "Gateway": "172.25.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]

$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
53961f344b90   bridge    bridge    local
d63af1f95612   host      host      local
6da6e75ceb17   net25     bridge    local
b98db7fe56a5   none      null      local

$ docker network rm net25 
net25
```

### Verificar que les xarxes definides si fan resolució de nom de container

Generalment els desplegaments de Docker no utilitzen mai la xarxa per defecte bridge i utilitzen xarxes personalitzades, el motiu és per segregra el desplegament  però també perquè aquestes xarxes si que fan resolució de noms de container.

Anem a repetir l’exemple de desplegament del servidor SSH i un host i a verificar que ara entre ells tenen connectivitat per adreça IP però també per nom de host.

```
$ docker network create mynet
e32e549cef460b8620caf3f099e9ee3797128ab52539f3de185e98ee0cc15d65

$ docker run --rm --name ssh.edt.org -h ssh.edt.org --net mynet  -d edtasixm05/ssh21
643baba09f57e80dcf10ffacf8ab7e1b0bce52746bffd53e1bf956ae5a78ec40

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS          PORTS     NAMES
643baba09f57   edtasixm05/ssh21   "/bin/sh -c '/etc/in…"   18 seconds ago   Up 17 seconds   22/tcp    ssh.edt.org
```

```
$ docker run --rm --name host1.edt.org -h host1.edt.org --net mynet  -it edtasixm05/ssh21 /bin/bash

root@host1:/tmp# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
133: eth0@if134: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:14:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.20.0.3/16 brd 172.20.255.255 scope global eth0
       valid_lft forever preferred_lft forever

root@host1:/tmp# ping -c1 172.20.0.2 
PING 172.20.0.2 (172.20.0.2) 56(84) bytes of data.
64 bytes from 172.20.0.2: icmp_seq=1 ttl=64 time=0.258 ms

--- 172.20.0.2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.258/0.258/0.258/0.000 ms

root@host1:/tmp# ping -c1 ssh.edt.org
PING ssh.edt.org (172.20.0.2) 56(84) bytes of data.
64 bytes from ssh.edt.org.mynet (172.20.0.2): icmp_seq=1 ttl=64 time=0.140 ms

--- ssh.edt.org ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.140/0.140/0.140/0.000 ms
```

  * El host client pot accedir al servidor SSH per nom de host.

```
root@host1:/tmp# ssh pere@ssh.edt.org
The authenticity of host 'ssh.edt.org (172.20.0.2)' can't be established.
ECDSA key fingerprint is SHA256:fWJSkoIkntn1ViVsYywFj0imWTIBKNmEeHcE8hT1k7c.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'ssh.edt.org,172.20.0.2' (ECDSA) to the list of known hosts.
pere@ssh.edt.org's password: pere
Linux ssh.edt.org 5.11.22-100.fc32.x86_64 #1 SMP Wed May 19 18:58:25 UTC 2021 x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.

$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
131: eth0@if132: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:14:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.20.0.2/16 brd 172.20.255.255 scope global eth0
       valid_lft forever preferred_lft forever

$ hostname
ssh.edt.org
 
$ ping -c1 host1.edt.org
PING host1.edt.org (172.20.0.3) 56(84) bytes of data.
64 bytes from host1.edt.org.mynet (172.20.0.3): icmp_seq=1 ttl=64 time=0.097 ms

--- host1.edt.org ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.097/0.097/0.097/0.000 ms

$ exit
Connection to ssh.edt.org closed.

root@host1:/tmp# hostname
host1.edt.org

root@host1:/tmp# exit
exit
```

```
$ docker stop ssh.edt.org 
ssh.edt.org
```


### Les xarxes estan isolates les unes de les altres (isolated)

Anem a verificar que els hosts d’una xarxa i una altra xarxa no es poden connectar entre ells per estar en xarxes separades.

  * Creem dues xarxes.

```
$ docker network create netA
1fdd39cea957a0de0c545b54af6de7caedd65d5a3550a5ea1bc3707a7c0b3b40

$ docker network create netB
b12dcb9daebc22b17c4ddba29a2962347e7109593e9aa2974637a530eaa12e16
```

  * A la xarxa netA despleguem el servidor SSH i un host.

```
$ docker run --rm --name ssh.edt.org -h ssh.edt.org --net netA  -d edtasixm05/ssh21
9255a380304cc286cccad76600931fd80595b98c9640e8ed6e0112b892e7427b

$ docker run --rm --name host1.edt.org -h host1.edt.org --net netA  -it edtasixm05/ssh21 /bin/bash
root@host1:/tmp# 
```

  * A la xarxa netB despleguem un servidor net21 i un host.

```
$ docker run --rm --name net.edt.org -h net.edt.org --net netB  -d edtasixm05/net21:base  
99fa28b28a884aea10dbfb3ec8bc6a4bd7ce5c0271abf709df0edf9a4e079214

$ docker run --rm --name host2.edt.org -h host2.edt.org --net netB  -it  edtasixm05/net21:base  /bin/bash
root@host2:/tmp# 
```

  * Observer des d’una altra sessió tot allò que tenim engegat.

```
$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED              STATUS              PORTS                   NAMES
c3ebfe601899   edtasixm05/net21:base   "/bin/bash"              50 seconds ago       Up 49 seconds       7/tcp, 13/tcp, 19/tcp   host2.edt.org
99fa28b28a88   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   About a minute ago   Up About a minute   7/tcp, 13/tcp, 19/tcp   net.edt.org
0ffbd0bb78d7   edtasixm05/ssh21        "/bin/bash"              3 minutes ago        Up 3 minutes        80/tcp                  host1.edt.org
9255a380304c   edtasixm05/ssh21        "/bin/sh -c '/etc/in…"   3 minutes ago        Up 3 minutes        80/tcp                  ssh.edt.org
```

  * Provem des de host1.edt.org accedir als altres containers.

```
root@host1:/tmp# hostname
Host1.edt.org

root@host1:/tmp# ping -c1 ssh.edt.org
PING ssh.edt.org (172.21.0.2) 56(84) bytes of data.
64 bytes from ssh.edt.org.netA (172.21.0.2): icmp_seq=1 ttl=64 time=0.219 ms

--- ssh.edt.org ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.219/0.219/0.219/0.000 ms

root@host1:/tmp# ping -c1 host1.edt.org
PING host1.edt.org (172.21.0.3) 56(84) bytes of data.
64 bytes from host1.edt.org (172.21.0.3): icmp_seq=1 ttl=64 time=0.059 ms

--- host1.edt.org ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.059/0.059/0.059/0.000 ms

root@host1:/tmp# ping -c1 net.edt.org
ping: net.edt.org: Name or service not known

root@host1:/tmp# ping -c1 host2.edt.org
ping: host2.edt.org: Name or service not known
```


  * Ara ho provem des de host2.edt.org. Viem que pot accedir als de la seva xarxa però no a l’altra.

```
root@host2:/tmp# hostname
host2.edt.org

root@host2:/tmp# ping -c1 net.edt.org
PING net.edt.org (172.22.0.2) 56(84) bytes of data.
64 bytes from net.edt.org.netB (172.22.0.2): icmp_seq=1 ttl=64 time=0.210 ms

--- net.edt.org ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.210/0.210/0.210/0.000 ms

root@host2:/tmp# ping -c1 host2.edt.org
PING host2.edt.org (172.22.0.3) 56(84) bytes of data.
64 bytes from host2.edt.org (172.22.0.3): icmp_seq=1 ttl=64 time=0.052 ms

--- host2.edt.org ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.052/0.052/0.052/0.000 ms

root@host2:/tmp# ping -c1 ssh.edt.org
ping: ssh.edt.org: Name or service not known

root@host2:/tmp# ping -c1 host1.edt.org
ping: host1.edt.org: Name or service not known
```

  * Ho tanquem tot…

```
$ docker stop ssh.edt.org net.edt.org host1.edt.org host2.edt.org 
ssh.edt.org
net.edt.org
host1.edt.org
host2.edt.org
```

### Practicar firewalls amb xarxes docker 

Anem a fer una petita bojeria i desplegar tres xarxes amb hosta a cada xarxa, xarxes netA, netB i netZ que simulen una orgaització amb dues xarxes internes d’usuaris  i una xarxa DMZ.

  * Crear les xarxes i engegar els 6 containers tots detach de cop.

```
docker network create netA
docker network create netB
docker network create netZ

docker run --rm --name hostA1 -h hostA1 --net netA --privileged -d edtasixm05/ssh21
docker run --rm --name hostA2 -h hostA2 --net netA --privileged -d edtasixm05/net21:base

docker run --rm --name hostB1 -h hostB1 --net netB --privileged -d edtasixm05/ssh21
docker run --rm --name hostB2 -h hostB2 --net netB --privileged -d edtasixm05/net21:base

docker run --rm --name z1 -h hostZ1 --net netZ --privileged -d edtasixm05/ssh21
docker run --rm --name z2 -h hostZ2 --net netZ --privileged -d edtasixm05/net21:base
```

```
$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED              STATUS              PORTS                   NAMES
bab98b46214f   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   5 seconds ago        Up 4 seconds        7/tcp, 13/tcp, 19/tcp   z2
3dea499dff48   edtasixm05/ssh21        "/bin/sh -c '/etc/in…"   5 seconds ago        Up 4 seconds        80/tcp                  z1
660b2264a83c   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   14 seconds ago       Up 13 seconds       7/tcp, 13/tcp, 19/tcp   hostB2
909830c5ee8a   edtasixm05/ssh21        "/bin/sh -c '/etc/in…"   14 seconds ago       Up 13 seconds       80/tcp                  hostB1
74553dbc8794   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   22 seconds ago       Up 21 seconds       7/tcp, 13/tcp, 19/tcp   hostA2
afea8c11af8d   edtasixm05/ssh21        "/bin/sh -c '/etc/in…"   About a minute ago   Up About a minute   80/tcp                  hostA1
```

  * Observem les regles de firewall i en especial les que Docker ha posat per impedir la comunicació entre xarxes diferents.

```
# iptables -L -t nat
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination         
DOCKER     all  --  anywhere             anywhere             ADDRTYPE match dst-type LOCAL

Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
DOCKER     all  --  anywhere            !127.0.0.0/8          ADDRTYPE match dst-type LOCAL

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         
MASQUERADE  all  --  172.23.0.0/16        anywhere            
MASQUERADE  all  --  172.22.0.0/16        anywhere            
MASQUERADE  all  --  172.21.0.0/16        anywhere            
MASQUERADE  all  --  172.20.0.0/16        anywhere            
MASQUERADE  all  --  172.17.0.0/16        anywhere            
LIBVIRT_PRT  all  --  anywhere             anywhere            

Chain DOCKER (2 references)
target     prot opt source               destination         
RETURN     all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            

Chain LIBVIRT_PRT (1 references)
target     prot opt source               destination         
RETURN     all  --  192.168.122.0/24     base-address.mcast.net/24 
RETURN     all  --  192.168.122.0/24     255.255.255.255     
MASQUERADE  tcp  --  192.168.122.0/24    !192.168.122.0/24     masq ports: 1024-65535
MASQUERADE  udp  --  192.168.122.0/24    !192.168.122.0/24     masq ports: 1024-65535
MASQUERADE  all  --  192.168.122.0/24    !192.168.122.0/24    
RETURN     all  --  192.168.100.0/24     base-address.mcast.net/24 
RETURN     all  --  192.168.100.0/24     255.255.255.255     
MASQUERADE  tcp  --  192.168.100.0/24    !192.168.100.0/24     masq ports: 1024-65535
MASQUERADE  udp  --  192.168.100.0/24    !192.168.100.0/24     masq ports: 1024-65535
MASQUERADE  all  --  192.168.100.0/24    !192.168.100.0/24    
```

  * Eliminem totes les regles i deixem totes les polítiques per defecte accept.

```
#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

#echo 1 > /proc/sys/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
```

  * Activem NAT, mascarade per a les xarxes internes.

```
# Fer NAT per les xarxes internes:
# - 172.21.0.0/24
# - 172.22.0.0/24
# - 172.23.0.0/24
iptables -t nat -A POSTROUTING -s 172.21.0.0/24 -o enp6s0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/24 -o enp6s0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.23.0.0/24 -o enp6s0 -j MASQUERADE
```

  * Ara tenen connectivitat entre elles i a l’exterior.

  * Tancar-ho tot

```
# docker stop hostA1 hostA2 hostB1 hostB2 z1 z2
hostA1
hostA2
hostB1
hostB2
z1
z2
```

---
---

### Exemples de funcionament de Network

S'utilitza una imatge amb un servidor/client ssh però que té també
instal·lades altres utilitats de xarxa per poder fer consultes de la
configuració de xarxa.

  * **ssh** Imatge servidor/client ssh amb algunes utilitats de xarxa.


#### Ordres Network

```
# docker network --help
Commands:
  connect     Connect a container to a network
  create      Create a network
  disconnect  Disconnect a container from a network
  inspect     Display detailed information on one or more networks
  ls          List networks
  prune       Remove all unused networks
  rm          Remove one or more networks
```

```
$ docker network create --help
Usage:  docker network create [OPTIONS] NETWORK
Create a network
Options:
      --attachable           Enable manual container attachment
      --aux-address map      Auxiliary IPv4 or IPv6 addresses used by Network driver (default map[])
      --config-from string   The network from which to copy the configuration
      --config-only          Create a configuration only network
  -d, --driver string        Driver to manage the Network (default "bridge")
      --gateway strings      IPv4 or IPv6 Gateway for the master subnet
      --ingress              Create swarm routing-mesh network
      --internal             Restrict external access to the network
      --ip-range strings     Allocate container ip from a sub-range
      --ipam-driver string   IP Address Management Driver (default "default")
      --ipam-opt map         Set IPAM driver specific options (default map[])
      --ipv6                 Enable IPv6 networking
      --label list           Set metadata on a network
  -o, --opt map              Set driver specific options (default map[])
      --scope string         Control the network's scope
      --subnet strings       Subnet in CIDR format that represents a network segment
```



```
# docker network ls
# docker network inspect bridge
# docker network inspect host
# docker network prune
```

```
docker network create alumnes
docker network ls
docker network inspect alumnes
docker network rm alumnes
```

```
$ docker network create --subnet 172.25.0.0/24 professors
a7b61e5e4a55f0f97235860596103008cee1da05b68eaa0f680983cb3923ccea

$ docker network ls
NETWORK ID     NAME         DRIVER    SCOPE
b73483af8c96   bridge       bridge    local
3cd5981ab9c5   host         host      local
44d09c45d440   none         null      local
a7b61e5e4a55   professors   bridge    local

$ docker network inspect professors 
[
    {
        "Name": "professors",
        "Id": "a7b61e5e4a55f0f97235860596103008cee1da05b68eaa0f680983cb3923ccea",
        "Created": "2022-05-27T12:49:16.585271642+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.25.0.0/24"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]

$ docker network rm professors
```

#### Exemple xarxa default bridge

```
$ docker run --rm --name ssh.edt.org -h ssh.edt.org -d edtasixm05/ssh21 

$ docker run --rm --name host1.edt.org -h host1.edt.org -it edtasixm05/ssh21  /bin/bash

root@host1:/tmp# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
12: eth0@if13: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

root@host1:/tmp# ping -c1 172.17.0.1
PING 172.17.0.1 (172.17.0.1) 56(84) bytes of data.
64 bytes from 172.17.0.1: icmp_seq=1 ttl=64 time=0.154 ms

root@host1:/tmp# ping -c1 172.17.0.2
PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
64 bytes from 172.17.0.2: icmp_seq=1 ttl=64 time=0.173 ms

root@host1:/tmp# ssh pere@172.17.0.2
The authenticity of host '172.17.0.2 (172.17.0.2)' can't be established.
ECDSA key fingerprint is SHA256:VW0DYyDTm3whlD2ZZ4n0usos9v3QR143TRcC+kwn8kY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.17.0.2' (ECDSA) to the list of known hosts.
pere@172.17.0.2's password: 
Linux ssh.edt.org 5.10.0-9-amd64 #1 SMP Debian 5.10.70-1 (2021-09-30) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.

$ hostname
ssh.edt.org

$ exit
Connection to 172.17.0.2 closed.

root@host1:/tmp# hostname
host1.edt.org
```

``` 
root@host1:/tmp# ping ssh.edt.org
ping: ssh.edt.org: Name or service not known
 
root@host1:/tmp# ssh pere@ssh.edt.org
ssh: Could not resolve hostname ssh.edt.org: Name or service not known
```

#### Exemple xarxa alumnes pròpia

```
$ docker network create alumnes
3484a2ab922c6725cc1034c80194b5ee42781654fc0642d0caf8fc6b71dc4d04

$ docker run --rm --name ssh.alumnes.org -h ssh.alumnes.org --net alumnes -d edtasixm05/ssh21
605f991161f1959f0b72a1fa6283fd515278fb000f148de8ad6780f7e227fa34

$ docker run --rm --name a1.alumnes.org -h a1.alumnes.org --net alumnes -it edtasixm05/ssh21 /bin/bash

root@a1:/tmp# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
21: eth0@if22: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:12:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.18.0.3/16 brd 172.18.255.255 scope global eth0
       valid_lft forever preferred_lft forever

root@a1:/tmp# ping -c 1  172.18.0.3
PING 172.18.0.3 (172.18.0.3) 56(84) bytes of data.
64 bytes from 172.18.0.3: icmp_seq=1 ttl=64 time=0.075 ms

root@a1:/tmp# ping -c 1  172.18.0.2
PING 172.18.0.2 (172.18.0.2) 56(84) bytes of data.
64 bytes from 172.18.0.2: icmp_seq=1 ttl=64 time=0.169 ms

root@a1:/tmp# ping -c 1  172.18.0.1
PING 172.18.0.1 (172.18.0.1) 56(84) bytes of data.
64 bytes from 172.18.0.1: icmp_seq=1 ttl=64 time=0.144 ms
```

```
root@a1:/tmp# ssh pere@172.18.0.2
The authenticity of host '172.18.0.2 (172.18.0.2)' can't be established.
ECDSA key fingerprint is SHA256:VW0DYyDTm3whlD2ZZ4n0usos9v3QR143TRcC+kwn8kY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.18.0.2' (ECDSA) to the list of known hosts.
pere@172.18.0.2's password: 
Linux ssh.alumnes.org 5.10.0-9-amd64 #1 SMP Debian 5.10.70-1 (2021-09-30) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
$ exit
Connection to 172.18.0.2 closed.
root@a1:/tmp# 
```

```
root@a1:/tmp# ping -c 1  ssh.alumnes.org
PING ssh.alumnes.org (172.18.0.2) 56(84) bytes of data.
64 bytes from ssh.alumnes.org.alumnes (172.18.0.2): icmp_seq=1 ttl=64 time=0.107 ms

root@a1:/tmp# ssh pere@ssh.alumnes.org
The authenticity of host 'ssh.alumnes.org (172.18.0.2)' can't be established.
ECDSA key fingerprint is SHA256:VW0DYyDTm3whlD2ZZ4n0usos9v3QR143TRcC+kwn8kY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'ssh.alumnes.org' (ECDSA) to the list of known hosts.
pere@ssh.alumnes.org's password: 
Linux ssh.alumnes.org 5.10.0-9-amd64 #1 SMP Debian 5.10.70-1 (2021-09-30) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Fri May 27 11:05:55 2022 from 172.18.0.3

$ hostname
ssh.alumnes.org

$ exit
Connection to ssh.alumnes.org closed.

root@a1:/tmp# hostname
a1.alumnes.org
root@a1:/tmp# 
```

```
$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS              PORTS     NAMES
38a7b66d7be3   m05ssh    "/bin/bash"              11 seconds ago       Up 9 seconds                  a1.alumnes.org
605f991161f1   m05ssh    "/bin/sh -c '/etc/in…"   About a minute ago   Up About a minute             ssh.alumnes.org
ee1398627ab3   m05ssh    "/bin/bash"              6 minutes ago        Up 6 minutes                  host1.edt.org
501e20e769b5   m05ssh    "/bin/sh -c '/etc/in…"   7 minutes ago        Up 7 minutes                  ssh.edt.org
```

```
root@a1:/tmp# ping -c1 172.17.0.2
PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
^C
--- 172.17.0.2 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

root@a1:/tmp# ping -c1 172.17.0.3
PING 172.17.0.3 (172.17.0.3) 56(84) bytes of data.
^C
--- 172.17.0.3 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms

root@a1:/tmp# ping -c1 ssh.edt.org
ping: ssh.edt.org: Name or service not known
root@a1:/tmp# 
root@a1:/tmp# ping -c1 host1.edt.org
ping: host1.edt.org: Name or service not known
```



#### Exemple xarxa professors personalitzada

```
$ docker network create --subnet=172.25.0.0/24 professors
8eb562b5200a50cbe247bb67a451ef20c70e1ec251bc42697ae73d466e9a262e

$ docker run --rm --name ssh.professors.org -h ssh.professors.org --net professors -d edtasixm05/ssh21
72449309ec26c15d06b7abab0007769f9cc090c2391a0aae61b20e843a11cfef

$ docker run --rm --name p1.professors.org -h p1.professors.org --net professors -it edtasixm05/ssh21 /bin/bash

root@p1:/tmp# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
28: eth0@if29: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:19:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.25.0.3/24 brd 172.25.0.255 scope global eth0
       valid_lft forever preferred_lft forever
```

```
root@p1:/tmp# ping -c 1 ssh.edt.org
ping: ssh.edt.org: Name or service not known

root@p1:/tmp# ping -c 1 ssh.alumnes.org
ping: ssh.alumnes.org: No address associated with hostname

root@p1:/tmp# ping -c 1 ssh.professors.org
PING ssh.professors.org (172.25.0.2) 56(84) bytes of data.
64 bytes from ssh.professors.org.professors (172.25.0.2): icmp_seq=1 ttl=64 time=0.138 ms

root@p1:/tmp# ssh pere@ssh.professors.org
The authenticity of host 'ssh.professors.org (172.25.0.2)' can't be established.
ECDSA key fingerprint is SHA256:VW0DYyDTm3whlD2ZZ4n0usos9v3QR143TRcC+kwn8kY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? no
Host key verification failed.
root@p1:/tmp# 
```

```
root@p1:/tmp# ping -c 1 google.com
PING google.com (142.250.200.142) 56(84) bytes of data.
64 bytes from mad41s14-in-f14.1e100.net (142.250.200.142): icmp_seq=1 ttl=112 time=10.7 ms

root@p1:/tmp# ping -c 1 escoladeltreball.org
PING escoladeltreball.org (10.1.1.8) 56(84) bytes of data.
64 bytes from 10.1.1.8 (10.1.1.8): icmp_seq=1 ttl=61 time=0.232 ms

root@p1:/tmp# apt-get install host

root@p1:/tmp# host escoladeltreball.org
escoladeltreball.org has address 10.1.1.8

root@p1:/tmp# host ssh.professors.org  
ssh.professors.org has address 172.25.0.2

root@p1:/tmp# host p1.professors.org 
p1.professors.org has address 172.25.0.3

root@p1:/tmp# host ssh.alumnes.org
ssh.alumnes.org mail is handled by 5 smtp.rzone.de. 
# fake response ...

root@p1:/tmp# host ssh.edt.org
Host ssh.edt.org not found: 3(NXDOMAIN)
```


#### Tancar-ho tot!

```
$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS     NAMES
67545153ccd7   m05ssh    "/bin/bash"              7 minutes ago    Up 7 minutes              p1.professors.org
72449309ec26   m05ssh    "/bin/sh -c '/etc/in…"   9 minutes ago    Up 9 minutes              ssh.professors.org
38a7b66d7be3   m05ssh    "/bin/bash"              19 minutes ago   Up 19 minutes             a1.alumnes.org
605f991161f1   m05ssh    "/bin/sh -c '/etc/in…"   20 minutes ago   Up 20 minutes             ssh.alumnes.org
ee1398627ab3   m05ssh    "/bin/bash"              26 minutes ago   Up 26 minutes             host1.edt.org
501e20e769b5   m05ssh    "/bin/sh -c '/etc/in…"   26 minutes ago   Up 26 minutes             ssh.edt.org

$ docker stop ssh.edt.org ssh.alumnes.org ssh.professors.org host1.edt.org a1.alumnes.org p1.professors.org 
ssh.edt.org
ssh.alumnes.org
ssh.professors.org
host1.edt.org
a1.alumnes.org
p1.professors.org
 
$ docker network rm alumnes professors
alumnes
professors

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```


