# getting started - Comptador - Compose 
## @edt ASIX-M05 Curs 2021-2022


### Exemple de Getting Startd - 1: Comptador de visites


Crear un servei web amb Flask amb un comptador de visites Redis. Mostra el nom
del host (en aquest cas el container) que es visita.

  * Pàgina web amb Flask
  * Comptador de visites Redis
  * Persistència del comptador amb volum


#### Prova-1: Engegar el servei web amb docker-compose

Engega només el servei web, amb Python i Flask. No funconarà el comptador
de visites perquè no hi ha el container Redis. Utilitza un fitxer docker-compose
que està dissenyat per ser usat en un stack o swarm, genera errors el *deploy*
però funciona engegant una instància.


```
$ docker-compose up -d
WARNING: Some services (web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Creating network "getstarted-1_webnet" with the default driver
Creating getstarted-1_web_1 ... done
 
$ docker-compose ps
WARNING: Some services (web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
       Name             Command      State                Ports              
-----------------------------------------------------------------------------
getstarted-1_web_1   python app.py   Up      0.0.0.0:80->80/tcp,:::80->80/tcp

$ docker-compose down
WARNING: Some services (web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Stopping getstarted-1_web_1 ... done
Removing getstarted-1_web_1 ... done
Removing network getstarted-1_webnet
```

#### Prova-2: Engegar aplicació web amb el comptador de visites Redis

Aquest exemple no té persistència de dades, el comptador de visites es perd en apagar el
desplegament.

```
$ docker-compose -f docker-compose-pas2-redis.yml up -d
WARNING: Some services (redis, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Creating network "getstarted-1_webnet" with the default driver
Pulling redis (redis:)...
Digest: sha256:1b90dbfe6943c72a7469c134cad3f02eb810f016049a0e19ad78be07040cdb0c
Status: Downloaded newer image for redis:latest
Creating getstarted-1_web_1   ... done
Creating getstarted-1_redis_1 ... done

$ docker-compose -f docker-compose-pas2-redis.yml ps
WARNING: Some services (redis, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
        Name                      Command               State                    Ports                  
--------------------------------------------------------------------------------------------------------
getstarted-1_redis_1   docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
getstarted-1_web_1     python app.py                    Up      0.0.0.0:80->80/tcp,:::80->80/tcp        
```

```
Accedir a localhost i refrescar la pàgina
```

```
$ docker-compose -f docker-compose-pas2-redis.yml  down
WARNING: Some services (redis, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Stopping getstarted-1_redis_1 ... done
Stopping getstarted-1_web_1   ... done
Removing getstarted-1_redis_1 ... done
Removing getstarted-1_web_1   ... done
Removing network getstarted-1_webnet
```

```
Repetirn el procés engegant de nou el servei.
El comptador de visites torna a començar de nou.
```

#### Prova-3: Afegir persitència al comptador de visites

Afegim un volum per proporcionar persistència de dades al comptador de 
visites Redis.

```
$ docker-compose -f docker-compose-pas3-redis-persistent.yml up -d
WARNING: Some services (redis, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Creating network "getstarted-1_webnet" with the default driver
Creating getstarted-1_web_1   ... done
Creating getstarted-1_redis_1 ... done
```

```
Verificar que tot i aturar i engegar el desplegament el comptador 
de visites perdura
```

```
$ docker-compose -f docker-compose-pas3-redis-persistent.yml down
WARNING: Some services (redis, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Stopping getstarted-1_redis_1 ... done
Stopping getstarted-1_web_1   ... done
Removing getstarted-1_redis_1 ... done
Removing getstarted-1_web_1   ... done
Removing network getstarted-1_webnet
```

#### Prova-4: Afegir un Visualizer

En aquest exemple en no formar part d'un swarm el Visualizer no mostrarà res

```
$ docker-compose -f docker-compose-pas4-visualizer.yml up -d
WARNING: Some services (redis, visualizer, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Creating network "getstarted-1_webnet" with the default driver
Creating getstarted-1_web_1        ... done
Creating getstarted-1_redis_1      ... done
Creating getstarted-1_visualizer_1 ... done

$ docker-compose -f docker-compose-pas4-visualizer.yml down
WARNING: Some services (redis, visualizer, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Stopping getstarted-1_redis_1      ... done
Stopping getstarted-1_web_1        ... done
Stopping getstarted-1_visualizer_1 ... done
Removing getstarted-1_redis_1      ... done
Removing getstarted-1_web_1        ... done
Removing getstarted-1_visualizer_1 ... done
Removing network getstarted-1_webnet
```


#### Prova-5: Afegir també un Portainer

Amb portainer podrem veure i administrar via web tot el desplegament

```
$ docker-compose -f docker-compose-pas5-portainer-visualizer.yml up -d
WARNING: Some services (portainer, redis, visualizer, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Creating network "getstarted-1_webnet" with the default driver
Creating getstarted-1_web_1        ... done
Creating getstarted-1_portainer_1  ... done
Creating getstarted-1_visualizer_1 ... done
Creating getstarted-1_redis_1      ... done

$ docker-compose -f docker-compose-pas5-portainer-visualizer.yml down
WARNING: Some services (portainer, redis, visualizer, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Stopping getstarted-1_redis_1      ... done
Stopping getstarted-1_visualizer_1 ... done
Stopping getstarted-1_portainer_1  ... done
Stopping getstarted-1_web_1        ... done
Removing getstarted-1_redis_1      ... done
Removing getstarted-1_visualizer_1 ... done
Removing getstarted-1_portainer_1  ... done
Removing getstarted-1_web_1        ... done
Removing network getstarted-1_webnet
```


