# compose:examples
## @edt ASIX-M05 Curs 2021-2022


### Treballar amb docker compose

  * Instal·lar compose
  * Desplegar un compose: up, down, -d, top, ps, etc
  * Detach -d
  * Nom per defecte: docker-compose
  * Nom específic: -f per a totes les odres


#### Desplegar manualment varis containers usant un script bash

```
$ cat script-net-portainer.sh 
#! /bin/bash
# @edt ASIX-M05 Curs 2021-2022
#
# Engegar els conatiners net21:base i portainer
docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:base
docker run --rm -p 9000:9000 --net mynet -v /var/run/docker.sock:/var/run/docker.sock -d portainer/portainer
docker ps

```
```
$ bash script-net-portainer.sh 
444ad065d063: Pull complete 
296e8f25b140: Pull complete 
CONTAINER ID   IMAGE                   COMMAND                  CREATED                  STATUS                  PORTS                                                                                                         NAMES
d0ad8d6a2303   portainer/portainer     "/portainer"             Less than a second ago   Up Less than a second   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp                                                                     cool_dijkstra
b61ed1503c59   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   2 seconds ago            Up Less than a second   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:80->80/tcp, :::80->80/tcp, 19/tcp   net.edt.org
[ecanet@mylaptop compose:examples]$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS         PORTS                                                                                                         NAMES
d0ad8d6a2303   portainer/portainer     "/portainer"             8 seconds ago    Up 7 seconds   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp                                                                     cool_dijkstra
b61ed1503c59   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   10 seconds ago   Up 8 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:80->80/tcp, :::80->80/tcp, 19/tcp   net.edt.org

$ docker stop cool_dijkstra net.edt.org 
cool_dijkstra
net.edt.org
```

#### Exemple-1 Docker-compose: net21 + visualizer

```
$ docker-compose up -d
Creating network "composeexamples_mynet" with the default driver
Creating net.edt.org                  ... done
Creating composeexamples_visualizer_1 ... done
$ 
$ docker-compose ps 
            Name                        Command             State                                  Ports                                
----------------------------------------------------------------------------------------------------------------------------------------
composeexamples_visualizer_1   npm start                    Up      0.0.0.0:8080->8080/tcp,:::8080->8080/tcp                            
net.edt.org                    /usr/sbin/xinetd -dontfork   Up      0.0.0.0:13->13/tcp,:::13->13/tcp, 0.0.0.0:19->19/tcp,:::19->19/tcp, 
                                                                    0.0.0.0:7->7/tcp,:::7->7/tcp            
```

```
$ docker-compose 
build    create   events   help     kill     pause    ps       push     rm       scale    stop     unpause  version  
config   down     exec     images   logs     port     pull     restart  run      start    top      up       


$ docker-compose top 
composeexamples_visualizer_1
UID    PID    PPID   C   STIME   TTY     TIME             CMD         
----------------------------------------------------------------------
root   7604   7569   0   21:12   ?     00:00:00   npm                 
root   7730   7604   0   21:12   ?     00:00:00   sh -c node server.js
root   7731   7730   0   21:12   ?     00:00:01   node server.js      

net.edt.org
UID    PID    PPID   C   STIME   TTY     TIME                CMD            
----------------------------------------------------------------------------
root   7593   7546   0   21:12   ?     00:00:00   /usr/sbin/xinetd -dontfork


$ docker-compose images 
         Container                    Repository           Tag       Image Id       Size  
------------------------------------------------------------------------------------------
composeexamples_visualizer_1   dockersamples/visualizer   stable   8dbf7c60cf88   148.3 MB
net.edt.org                    edtasixm05/net21           base     b3bf496b62b2   323.6 MB
```

```
$ docker-compose down 
Stopping composeexamples_visualizer_1 ... done
Stopping net.edt.org                  ... done
Removing composeexamples_visualizer_1 ... done
Removing net.edt.org                  ... done
Removing network composeexamples_mynet
```


#### Exemple-2 Docker-compose: net21 + portainer


```
$ docker-compose -f docker-compose-v2-net21-portainer.yml up -d
Creating net.edt.org                 ... done
Creating composeexamples_portainer_1 ... done


$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                       NAMES
9533381bf664   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   11 seconds ago   Up 10 seconds   7/tcp, 13/tcp, 19/tcp                       net.edt.org
a631b51986ea   portainer/portainer     "/portainer"             11 seconds ago   Up 10 seconds   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp   composeexamples_portainer_1
```

Atenció a usar el nom correcte!
```
$ docker-compose ps 
   Name                Command             State           Ports        
------------------------------------------------------------------------
net.edt.org   /usr/sbin/xinetd -dontfork   Up      13/tcp, 19/tcp, 7/tcp

$ docker-compose -f docker-compose-v2-net21-portainer.yml ps
           Name                        Command             State                    Ports                  
-----------------------------------------------------------------------------------------------------------
composeexamples_portainer_1   /portainer                   Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
net.edt.org                   /usr/sbin/xinetd -dontfork   Up      13/tcp, 19/tcp, 7/tcp                   
``` 

```
$ docker-compose -f docker-compose-v2-net21-portainer.yml down
Stopping composeexamples_portainer_1 ... done
Stopping net.edt.org                 ... done
Removing composeexamples_portainer_1 ... done
Removing net.edt.org                 ... done
Removing network composeexamples_mynet
```


#### Exemple-3 Docker-compose: lpdap + portainer


```
$ docker-compose -f docker-compose-v2-ldap-portainer.yml up -d
Creating network "v2_mynet" with the default driver
Pulling ldap (edtasixm06/ldapserver:18group)...
Status: Downloaded newer image for edtasixm06/ldapserver:18group
Creating v2_portainer_1 ... done
Creating ldap.edt.org   ... done


$ docker-compose -f docker-compose-v2-ldap-portainer.yml ps
     Name               Command           State                    Ports                  
------------------------------------------------------------------------------------------
ldap.edt.org     /opt/docker/startup.sh   Up      0.0.0.0:389->389/tcp,:::389->389/tcp    
v2_portainer_1   /portainer               Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
 
$ docker-compose -f docker-compose-v2-ldap-portainer.yml top
ldap.edt.org
UID     PID    PPID    C   STIME   TTY     TIME                   CMD               
------------------------------------------------------------------------------------
root   14675   14629   0   21:49   ?     00:00:00   /bin/bash /opt/docker/startup.sh
root   14817   14675   0   21:49   ?     00:00:00   /sbin/slapd -d0                 

v2_portainer_1
UID     PID    PPID    C   STIME   TTY     TIME        CMD    
--------------------------------------------------------------
root   14693   14658   0   21:49   ?     00:00:00   /portainer
```

```
$ ldapsearch -x -LLL -h localhost -b 'dc=edt,dc=org' -s base
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org
```

```
$ docker-compose -f docker-compose-v2-ldap-portainer.yml down
Stopping ldap.edt.org   ... done
Stopping v2_portainer_1 ... done
Removing ldap.edt.org   ... done
Removing v2_portainer_1 ... done
Removing network v2_mynet
```


#### Exemple-4 Docker-compose: net21 + web21 + portainer


```
$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml up -d
Creating network "v2_mynet" with the default driver
Pulling web (edtasixm05/web21:base)...
Status: Downloaded newer image for edtasixm05/web21:base
Creating web.edt.org    ... done
Creating v2_portainer_1 ... done
Creating net.edt.org    ... done


$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml ps
     Name                   Command               State                    Ports                  
--------------------------------------------------------------------------------------------------
net.edt.org      /usr/sbin/xinetd -dontfork       Up      13/tcp, 19/tcp, 7/tcp                   
v2_portainer_1   /portainer                       Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
web.edt.org      /bin/sh -c /opt/docker/sta ...   Up      80/tcp                                  


$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml top
net.edt.org
UID     PID    PPID    C   STIME   TTY     TIME                CMD            
------------------------------------------------------------------------------
root   15991   15952   0   21:55   ?     00:00:00   /usr/sbin/xinetd -dontfork

v2_portainer_1
UID     PID    PPID    C   STIME   TTY     TIME        CMD    
--------------------------------------------------------------
root   16042   16014   0   21:55   ?     00:00:00   /portainer

web.edt.org
 UID      PID    PPID    C   STIME   TTY     TIME                   CMD               
--------------------------------------------------------------------------------------
root     15975   15927   0   21:55   ?     00:00:00   /bin/bash /opt/docker/startup.sh
root     16175   15975   0   21:55   ?     00:00:00   /sbin/httpd -DFOREGROUND        
apache   16199   16175   0   21:55   ?     00:00:00   /sbin/httpd -DFOREGROUND        
apache   16200   16175   0   21:55   ?     00:00:00   /sbin/httpd -DFOREGROUND        
apache   16201   16175   0   21:55   ?     00:00:00   /sbin/httpd -DFOREGROUND        
apache   16203   16175   0   21:55   ?     00:00:00   /sbin/httpd -DFOREGROUND    

$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml down
Stopping net.edt.org    ... done
Stopping v2_portainer_1 ... done
Stopping web.edt.org    ... done
Removing net.edt.org    ... done
Removing v2_portainer_1 ... done
Removing web.edt.org    ... done
Removing network v2_mynet
```




