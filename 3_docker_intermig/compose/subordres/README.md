# Compose - Subordres 
## @edt ASIX-M05 Curs 2021-2022


### Exemples de sub ordres de  Docker-compose

```
$ docker-compose 
build    down     help     logs     ps       restart  scale    top      version  
config   events   images   pause    pull     rm       start    unpause  
create   exec     kill     port     push     run      stop     up       
```

 * up -d
 * down
 * ps
 * top [service]
 * images
 * push
 * pull
 * logs [service]
 * port [service]
 * pause
 * unpause
 * stop
 * start
 * --scale service=nº


#### Exemples-1

```
$ docker-compose up -d
Creating network "subordres_webnet" with the default driver
Creating subordres_portainer_1  ... done
Creating subordres_web_1        ... done
Creating subordres_visualizer_1 ... done
Creating subordres_redis_1      ... done

$ docker-compose ps
         Name                       Command               State                    Ports                  
----------------------------------------------------------------------------------------------------------
subordres_portainer_1    /portainer                       Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
subordres_redis_1        docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
subordres_visualizer_1   npm start                        Up      0.0.0.0:8080->8080/tcp,:::8080->8080/tcp
subordres_web_1          python app.py                    Up      0.0.0.0:80->80/tcp,:::80->80/tcp      

$ docker-compose top
subordres_portainer_1
UID     PID    PPID    C   STIME   TTY     TIME        CMD    
--------------------------------------------------------------
root   20148   20102   0   21:05   ?     00:00:00   /portainer

subordres_redis_1
  UID       PID    PPID    C   STIME   TTY     TIME             CMD        
---------------------------------------------------------------------------
systemd+   20126   20072   0   21:05   ?     00:00:00   redis-server *:6379

subordres_visualizer_1
UID     PID    PPID    C   STIME   TTY     TIME             CMD         
------------------------------------------------------------------------
root   20004   19957   1   21:05   ?     00:00:00   npm                 
root   20363   20004   0   21:05   ?     00:00:00   sh -c node server.js
root   20364   20363   0   21:05   ?     00:00:00   node server.js      

subordres_web_1
UID     PID    PPID    C   STIME   TTY     TIME          CMD     
-----------------------------------------------------------------
root   20013   19981   0   21:05   ?     00:00:00   python app.py
```

```
 docker-compose top visualizer
subordres_visualizer_1
UID     PID    PPID    C   STIME   TTY     TIME             CMD         
------------------------------------------------------------------------
root   20004   19957   0   21:05   ?     00:00:00   npm                 
root   20363   20004   0   21:05   ?     00:00:00   sh -c node server.js
root   20364   20363   0   21:05   ?     00:00:00   node server.js      
$ 
$ docker-compose top web
subordres_web_1
UID     PID    PPID    C   STIME   TTY     TIME          CMD     
-----------------------------------------------------------------
root   20013   19981   0   21:05   ?     00:00:00   python app.py
```


#### Exemples-2


```
$ docker-compose images
      Container                 Repository             Tag        Image Id       Size  
---------------------------------------------------------------------------------------
subordres_portainer_1    portainer/portainer        latest      580c0e4e98b0   79.09 MB
subordres_redis_1        redis                      latest      53aa81e8adfa   116.8 MB
subordres_visualizer_1   dockersamples/visualizer   stable      8dbf7c60cf88   148.3 MB
subordres_web_1          edtasixm05/getstarted      comptador   87c8e3c95b10   158.6 MB

$ docker-compose push

$ docker-compose pull
Pulling web        ... done
Pulling redis      ... done
Pulling visualizer ... done
Pulling portainer  ... done
```

#### Exemples-3

```
$ docker-compose logs
WARNING: Some services (portainer, redis, visualizer, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Attaching to subordres_visualizer_1, subordres_redis_1, subordres_web_1, subordres_portainer_1
redis_1       | 1:C 31 May 2022 19:05:48.733 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
redis_1       | 1:C 31 May 2022 19:05:48.733 # Redis version=7.0.0, bits=64, commit=00000000, modified=0, pid=1, just started
redis_1       | 1:C 31 May 2022 19:05:48.733 # Configuration loaded
redis_1       | 1:M 31 May 2022 19:05:48.735 * monotonic clock: POSIX clock_gettime
redis_1       | 1:M 31 May 2022 19:05:48.737 * Running mode=standalone, port=6379.
redis_1       | 1:M 31 May 2022 19:05:48.738 # Server initialized
redis_1       | 1:M 31 May 2022 19:05:48.738 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
redis_1       | 1:M 31 May 2022 19:05:48.738 * The AOF directory appendonlydir doesn't exist
redis_1       | 1:M 31 May 2022 19:05:48.744 * SYNC append only file rewrite performed
redis_1       | 1:M 31 May 2022 19:05:48.754 * Ready to accept connections
web_1         |  * Serving Flask app "app" (lazy loading)
web_1         |  * Environment: production
web_1         |    WARNING: This is a development server. Do not use it in a production deployment.
web_1         |    Use a production WSGI server instead.
web_1         |  * Debug mode: off
web_1         |  * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)
portainer_1   | 2022/05/31 19:05:48 Warning: the --template-file flag is deprecated and will likely be removed in a future version of Portainer.
...

$ docker-compose logs visualizer 
WARNING: Some services (portainer, redis, visualizer, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Attaching to subordres_visualizer_1
visualizer_1  | npm info it worked if it ends with ok
visualizer_1  | npm info using npm@5.3.0
visualizer_1  | npm info using node@v8.2.1
visualizer_1  | npm info lifecycle swarmVisualizer@0.0.1~prestart: swarmVisualizer@0.0.1
visualizer_1  | npm info lifecycle swarmVisualizer@0.0.1~start: swarmVisualizer@0.0.1
visualizer_1  | 
visualizer_1  | > swarmVisualizer@0.0.1 start /app
visualizer_1  | > node server.js
visualizer_1  | 
visualizer_1  | undefined
```

```
$ docker-compose port web 80
0.0.0.0:80

$ docker-compose port visualizer 8080
0.0.0.0:8080
```

#### Exemple-4

```
$ docker-compose pause visualizer 
Pausing subordres_visualizer_1 ... done

$ docker-compose ps
         Name                       Command               State                     Ports                  
-----------------------------------------------------------------------------------------------------------
subordres_portainer_1    /portainer                       Exit 1                                           
subordres_redis_1        docker-entrypoint.sh redis ...   Up       0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
subordres_visualizer_1   npm start                        Paused   0.0.0.0:8080->8080/tcp,:::8080->8080/tcp
subordres_web_1          python app.py                    Up       0.0.0.0:80->80/tcp,:::80->80/tcp        

$ docker-compose unpause visualizer 
Unpausing subordres_visualizer_1 ... done

$ docker-compose ps
         Name                       Command               State                     Ports                  
-----------------------------------------------------------------------------------------------------------
subordres_portainer_1    /portainer                       Exit 1                                           
subordres_redis_1        docker-entrypoint.sh redis ...   Up       0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
subordres_visualizer_1   npm start                        Up       0.0.0.0:8080->8080/tcp,:::8080->8080/tcp
subordres_web_1          python app.py                    Up       0.0.0.0:80->80/tcp,:::80->80/tcp       
```

```
$ docker-compose stop visualizer 
Stopping subordres_visualizer_1 ... done

$ docker-compose ps
         Name                       Command               State                     Ports                  
-----------------------------------------------------------------------------------------------------------
subordres_portainer_1    /portainer                       Exit 1                                           
subordres_redis_1        docker-entrypoint.sh redis ...   Up       0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
subordres_visualizer_1   npm start                        Exit 0                                           
subordres_web_1          python app.py                    Up       0.0.0.0:80->80/tcp,:::80->80/tcp       

$ docker-compose start visualizer portainer 
Starting visualizer ... done
Starting portainer  ... done

$ docker-compose ps
         Name                       Command               State                    Ports                  
----------------------------------------------------------------------------------------------------------
subordres_portainer_1    /portainer                       Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
subordres_redis_1        docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
subordres_visualizer_1   npm start                        Up      0.0.0.0:8080->8080/tcp,:::8080->8080/tcp
subordres_web_1          python app.py                    Up      0.0.0.0:80->80/tcp,:::80->80/tcp   
```


### Rèpliques d'un mateix servei: scale

En aquest exemple veurem com amb docker-compose podem desplegar varies rèpliques
d'un mateix component, un servei. En aquest cas s'utilitzarà el servei web. 
Usualment s'utilitzarà *docker service* o *docker stack* per gestionar deplyments
més complexos.

**Problema:** usant docker-compose es desplegen serveis (containers) però aquests són 
apropiatius dels recursos, per exemple dels ports que utilitzen. Així per exemple si
s'intenten desplegar tres rèpliques del servei web que utilitza el port 80 no es podrà,
perquè en desplegar la primera rèplica aquesta ja fa el lligam (bind) amb el port 80
del host amfitriò i les altres rèpliques ja no el poden usar, hi ha un conflicte per 
apropiar-se del recurs.

**Exemple de conflicte de ports**

```
$ docker-compose ps web 
     Name            Command      State                Ports              
--------------------------------------------------------------------------
subordres_web_1   python app.py   Up      0.0.0.0:80->80/tcp,:::80->80/tcp
```
 
```
$ docker-compose scale web=2
WARNING: The scale command is deprecated. Use the up command with the --scale flag instead.
WARNING: The "web" service specifies a port on the host. If multiple containers for this service are created on a single host, the port will clash.
Starting subordres_web_1 ... done
Creating subordres_web_2 ... 
Creating subordres_web_2 ... error

ERROR: for subordres_web_2  Cannot start service web: driver failed programming external connectivity on endpoint subordres_web_2 (33f79806c7ebcc78d51c5b10090ac9ce73e887b024b5b81308e1c8f98b563506): Bind for 0.0.0.0:80 failed: port is already allocated
ERROR: Cannot start service web: driver failed programming external connectivity on endpoint subordres_web_2 (33f79806c7ebcc78d51c5b10090ac9ce73e887b024b5b81308e1c8f98b563506): Bind for 0.0.0.0:80 failed: port is already allocated

$ docker-compose down
```

**Implementar serveis amb ports dinàmics**


Una primera aproximació és desplegar varies rèpliques del servei web usant la 
publicació de port dinàmic. En aquest cas cada rèplica propagarà el seu port 80 a 
un port dinàmic diferent del host amfitriò.

L'**inconvenient** és que ara el servei web no està al port 80 sinó a tres ports dinàmics
que cal identificar. Es pot comprovar que accedint a localhost de cada un d'aquests 
ports es mostra la pàgina web i el comptador de visites (que s'incrementa conjuntament).

**Atenció**, si es volen desplegar vàries rèpliques i que totes elles atenguin al port
80 caldrà usar una *routing mesh* com la que proporciona automàticament *docker stack* 
amb *docker swarm*.

```
$ docker-compose -f docker-compose-noports.yml up -d --scale web=3
Creating network "subordres_webnet" with the default driver
Creating subordres_redis_1      ... done
Creating subordres_portainer_1  ... done
Creating subordres_visualizer_1 ... done
Creating subordres_web_1        ... done
Creating subordres_web_2        ... done
Creating subordres_web_3        ... done

$ docker-compose -f docker-compose-noports.yml ps
         Name                       Command               State                    Ports                  
----------------------------------------------------------------------------------------------------------
subordres_portainer_1    /portainer                       Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
subordres_redis_1        docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
subordres_visualizer_1   npm start                        Up      0.0.0.0:8080->8080/tcp,:::8080->8080/tcp
subordres_web_1          python app.py                    Up      0.0.0.0:49153->80/tcp,:::49153->80/tcp  
subordres_web_2          python app.py                    Up      0.0.0.0:49155->80/tcp,:::49155->80/tcp  
subordres_web_3          python app.py                    Up      0.0.0.0:49154->80/tcp,:::49154->80/tcp  
```

```
# Verificar la web i el comptador de visites al port indicat
```

**Modificar el desplegament actual 'en calent'**

```
$ docker-compose -f docker-compose-noports.yml up -d --scale web=2
subordres_redis_1 is up-to-date
subordres_visualizer_1 is up-to-date
Starting subordres_portainer_1        ... done
Stopping and removing subordres_web_3 ... done
Starting subordres_web_1              ... done
Starting subordres_web_2              ... done

$ docker-compose ps 
         Name                       Command               State                    Ports                  
----------------------------------------------------------------------------------------------------------
subordres_portainer_1    /portainer                       Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
subordres_redis_1        docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp,:::6379->6379/tcp
subordres_visualizer_1   npm start                        Up      0.0.0.0:8080->8080/tcp,:::8080->8080/tcp
subordres_web_1          python app.py                    Up      0.0.0.0:49153->80/tcp,:::49153->80/tcp  
subordres_web_2          python app.py                    Up      0.0.0.0:49155->80/tcp,:::49155->80/tcp  

$ docker-compose -f docker-compose-noports.yml down
WARNING: Some services (portainer, redis, visualizer, web) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Stopping subordres_web_1        ... done
Stopping subordres_web_2        ... done
Stopping subordres_visualizer_1 ... done
Stopping subordres_portainer_1  ... done
Stopping subordres_redis_1      ... done
Removing subordres_web_1        ... done
Removing subordres_web_2        ... done
Removing subordres_visualizer_1 ... done
Removing subordres_portainer_1  ... done
Removing subordres_redis_1      ... done
Removing network subordres_webnet
```




