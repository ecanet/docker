# docker
## @edt ASIX-M05 Curs 2021-2022

## Exemples de Dockerfile

 * mydebian
 * myweb


 * web21
 * ssh21
 * net21


 * samba21
 * postgres21
 * ldap21

---
---

# Dockerfile (format)


En aquesta secció descriurem com són els fitxers de generació d’imatges Dockerfile i proposarem casos pràctics per generar imatges de serveis de xarxa.


El millor mecanisme per consultar el dformat dels fitxers Dockerfile és a través de **Docker Documetation** a la secció **Reference Documentation**, apartat **File Format**, entrada **Dockerfile Reference**. 

Podeu observar que és el clàssic document Reference que desciu completament tota la sintaxis d’un fitxer Dockerfile. Indica que el temps estimat de lectura són 87 minuts.


Les principals directives són

**FROM**

The FROM instruction initializes a new build stage and sets the Base Image for subsequent instructions. As such, a valid Dockerfile must start with a FROM instruction. The image can be any valid image – it is especially easy to start by pulling an image from the Public Repositories.

**LABEL**

The LABEL instruction adds metadata to an image. A LABEL is a key-value pair. To include spaces within a LABEL value, use quotes and backslashes as you would in command-line parsing. 
  * LABEL <key>=<value> <key>=<value> <key>=<value> ...

**RUN**

UN has 2 forms:
  * RUN <command> (shell form, the command is run in a shell, which by default is /bin/sh -c on Linux or cmd /S /C on Windows)
  * RUN ["executable", "param1", "param2"] (exec form)

The RUN instruction will execute any commands in a new layer on top of the current image and commit the results. The resulting committed image will be used for the next step in the Dockerfile.

**ENV**

The ENV instruction sets the environment variable <key> to the value <value>. This value will be in the environment for all subsequent instructions in the build stage and can be replaced inline in many as well. The value will be interpreted for other environment variables, so quote characters will be removed if they are not escaped. Like command line parsing, quotes and backslashes can be used to include spaces within values.
  * ENV <key>=<value> ...

**COPY**

COPY has two forms:
  * COPY [--chown=<user>:<group>] <src>... <dest>
  * COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]
This latter form is required for paths containing whitespace. The COPY instruction copies new files or directories from <src> and adds them to the filesystem of the container at the path <dest>.

Multiple <src> resources may be specified but the paths of files and directories will be interpreted as relative to the source of the context of the build.

Each <src> may contain wildcards and matching will be done using Go’s filepath.Match rules.

**ADD**

ADD has two forms:
  * ADD [--chown=<user>:<group>] <src>... <dest>
  * ADD [--chown=<user>:<group>] ["<src>",... "<dest>"]
The latter form is required for paths containing whitespace. The ADD instruction copies new files, directories or remote file URLs from <src> and adds them to the filesystem of the image at the path <dest>. 

Multiple <src> resources may be specified but if they are files or directories, their paths are interpreted as relative to the source of the context of the build.

Each <src> may contain wildcards and matching will be done using Go’s filepath.Match rules.

**WORKDIR** 

The WORKDIR instruction sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile. If the WORKDIR doesn’t exist, it will be created even if it’s not used in any subsequent Dockerfile instruction.

The WORKDIR instruction can be used multiple times in a Dockerfile. If a relative path is provided, it will be relative to the path of the previous WORKDIR instruction.
  * WORKDIR /path/to/workdir

**CMD**

The CMD instruction has three forms:
  * CMD ["executable","param1","param2"] (exec form, this is the preferred form)
  * CMD ["param1","param2"] (as default parameters to ENTRYPOINT)
  * CMD command param1 param2 (shell form)

There can only be one CMD instruction in a Dockerfile. If you list more than one CMD then only the last CMD will take effect.

The main purpose of a CMD is to provide defaults for an executing container. These defaults can include an executable, or they can omit the executable, in which case you must specify an ENTRYPOINT instruction as well.

**ENTRYPOINT**

ENTRYPOINT has two forms:
  * The exec form, which is the preferred form:
    ENTRYPOINT ["executable", "param1", "param2"]
  * The shell form:
    ENTRYPOINT command param1 param2

An ENTRYPOINT allows you to configure a container that will run as an executable. Command line arguments to docker run <image> will be appended after all elements in an exec form ENTRYPOINT, and will override all elements specified using CMD. This allows arguments to be passed to the entry point.

**EXPOSE**

The EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime. You can specify whether the port listens on TCP or UDP, and the default is TCP if the protocol is not specified.

The EXPOSE instruction does not actually publish the port. It functions as a type of documentation between the person who builds the image and the person who runs the container, about which ports are intended to be published. 
  * EXPOSE <port> [<port>/<protocol>...]

**VOLUME**

The VOLUME instruction creates a mount point with the specified name and marks it as holding externally mounted volumes from native host or other containers. The value can be a JSON array, VOLUME ["/var/log/"], or a plain string with multiple arguments, such as VOLUME /var/log or VOLUME /var/log /var/db. 
  * VOLUME ["/data"]

**USER**

The USER instruction sets the user name (or UID) and optionally the user group (or GID) to use when running the image and for any RUN, CMD and ENTRYPOINT instructions that follow it in the Dockerfile.
  * USER <user>[:<group>]
  * USER <UID>[:<GID>]

**HEALTHCHECK**

The HEALTHCHECK instruction has two forms:
  * HEALTHCHECK [OPTIONS] CMD command (check container health by running a command inside the container)
  * HEALTHCHECK NONE (disable any healthcheck inherited from the base image)
The HEALTHCHECK instruction tells Docker how to test a container to check that it is still working. This can detect cases such as a web server that is stuck in an infinite loop and unable to handle new connections, even though the server process is still running.

When a container has a healthcheck specified, it has a health status in addition to its normal status. This status is initially starting. Whenever a health check passes, it becomes healthy (whatever state it was previously in). After a certain number of consecutive failures, it becomes unhealthy.


